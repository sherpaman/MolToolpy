import phrases
import numpy 

if sys.version_info[0] < 3:
    import Tkinter as Tk
    import ttk
    import tkFileDialog as filedialog
else:
    import tkinter as Tk
    from tkinter import ttk
    from tkinter import filedialog


class LabelInput(tk.Frame):
    def __init__(self, parent, label='', input_class=ttk.Entry, input_var=None, input_args=None, label_args=None, input_list=None **kwargs):
        super().__init__(parent, **kwargs)
        input_args = input_args or {}
        label_args = label_args or {}
        input_list = input_list or []
        if input_class in (ttk.Checkbutton, ttk.Button, ttk.Radiobutton):
            input_args['text'] = label
            input_args['variable'] = input_var
        else:
            self.label = tkk.Label(self, text=label, **label_args)
            self.label.grid(row=0, column=0)
            input_args['textvariable'] = input_var
        self.input = input_class(self, *input_list, **input_args)
        self.input.grid(row=1, column=0)
        if 'update' in kwargs:
            self.update = tkk.Label(self, text = kwargs['update'])
            self.update.grid(row=1,column=1)
        self.columnconfigure(0,weight=1)
        
    def grid(self, sticky=(tk.W + tk.E), **kwargs):
        super().grid(sticky=sticky,**kwargs)
    
    def get(self):
        try:
            if self.variable:
                return self.variable.get()
            elif type(self.input) == Tk.Text:
                return self.input.get('0.0',tk.END)
            else:
                return self.input.get()
        except:
            return ''
    
    def set(self, value, *args, **kwargs):
        if type(self.variable) == tk.BooleanVar:
            self.variable.set(bool(value))
        elif self.variable:
            self.variable.set(value, *args, **kwargs)
        elif type(self.input) in (ttk.Checkbutton,
            ttk.Radiobutton):
            if value:
                self.input.select()
            else:
                self.input.deselect()
        elif type(self.input) == tk.Text:
            self.input.delete('1.0', tk.END)
            self.input.insert('1.0', value)
        else: # input must be an Entry-type widget with no variable
            self.input.delete(0, tk.END)
            self.input.insert(0, value)
    
    
class SelectFileButton(tk.Frame):
    def __init__(self, parent, label='', file_name=Tk.StringVar(), file_type=None, **kwargs):
        super().__init__(parent, **kwargs)
        self.file_name = file_name
        self.file_basename = Tk.StringVar() 
        self.file_type = file_type 
        self.label  = tkk.Label(self, textvariable=self.file_name, **label_args)
        self.button = ttk.Button(self,text=label,command=self._open_file_menu(self.file_type))
        self.label.grid(row=0, column=0)
        self.button.grid(row=1, column=0)
        self.columnconfigure(0,weight=1)
    
    def grid(self, sticky=(tk.W + tk.E), **kwargs):
        super().grid(sticky=sticky,**kwargs)
    
    def get(self):
        return self.file_name
    
    def _open_file_menu(self,t):
        def f():
            self.file_name.set(filedialog.askopenfilename(title="{0:s} file".format(t),filetypes = self.file_type))
            self.file_basename.set(os.path.basename(self.file_name.get()))
        return f

class ReadTrajGUI(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        
        self.files = { 'traj' :  [('XTC', '*.xtc'), ('All files', '*')],
                       'topol':  [('TPR', '*.tpr'), ('All files', '*')]}
        self.file_names     = { ft : Tk.StringVar() for ft in self.files }
        self.file_basenames = { ft : Tk.StringVar() for ft in self.files }
        self.file_out  = Tk.StringVar()
        
        self.par       = { 'b'   : Tk.IntVar(),
                           'e'     : Tk.IntVar(),
                           's'    : Tk.IntVar(),
                           'group'   : Tk.StringVar() ,
                           'grp_num' : Tk.IntVar() ,
                           'cutoff' : Tk.DoubleVar() , 
                           'rec' : Tk.StringVar() , 
                           'lig' : Tk.StringVar() }
                        
        self.opt       = { 'do_parallel' : Tk.BooleanVar() }
        
        self.group_opt = [ "res", "all", "atom", "num_atoms" ]
        
        self.inputs = {}
        
        self.Files        = tk.LabelFrame(self, text = "Files")
        self.Selection    = tk.LabelFrame(self, text = "Selection")
        self.ReadOptions  = tk.LabelFrame(self, text = "Read Options")
        self.OtherOptions = tk.LabelFrame(self, text = "Other Options")
        self.Run          = ttk.Button(self, text="Run", command = self.run )

        #Files Form
        for n,ft in enumerate(self.files):
            self.inputs[ft] = SelectFileButton(Files, label=ft, file_name = self.file_names[ft], file_type = self.file_types[ft])
            self.inputs[ft].grid(row=n,column=0)
        
        self.inputs['output'] = LabelInput(Files,label='Output',input_var=self.file_out)
        self.inputs['output'].grid(row=n+1,column=0)
        
        #Selection Options
        self.rec_atoms = Tk.StringVar()
        self.lig_atoms = Tk.StringVar()
        self.inputs['rec']  = LabelInput(Selection, label='receptor', input_var=self.par['rec'], update_val = self.rec_atoms )
        self.inputs['lig']    = LabelInput(Selection, label='ligand',   input_var=self.par['lig'], update_val = self.lig_atoms )
        self.inputs['check_sel'] = LabelInput(Selection, label='Check Selections', input_class = ttk.Button, command = self.check_sel )
        
        self.inputs['rec'].grid(row=0,column=0) 
        self.inputs['lig'].grid(row=1,column=0)   
        self.inputs['check_sel'].grid(row=2,column=0)
        
        #Reading Option
        self.inputs['b']       = LabelInput(ReadOptions, label='begin', input_var=self.par['b'])
        self.inputs['e']         = LabelInput(ReadOptions, label='end', input_var=self.par['e'])
        self.inputs['s']        = LabelInput(ReadOptions, label='skip', input_var=self.par['s'])
        self.inputs['do_parallel'] = LabelInput(ReadOptions, input_class = ttk.Checkbutton, label="Parallel Routines", input_var = self.opt['do_parallel'])
        
        self.inputs['b'].grid(row=0,column=0)      
        self.inputs['e'].grid(row=1,column=0)        
        self.inputs['s'].grid(row=2,column=0)       
        self.inputs['do_parallel'].grid(row=3,column=0)
        
        #Other Options
        self.inputs['cutoff']     = LabelInput(ReadOptions, label='Distance cut-off [Å]', input_var = self.par['cutoff'])
        self.inputs['group']       = LabelInput(ReadOptions, input_class = ttk.ComboBox, label='Grouping Method', input_var = self.par['group'], input_args ={ 'values': self.group_opt } )
        self.inputs['grp_num']     = LabelInput(ReadOptions, label='Grouping number', input_var = self.par['grp_num'])
        
        self.inputs['cutoff'].grid(row=0,column=0)
        self.inputs['group'].grid(row=1,column=0)
        self.inputs['grp_num'].grid(row=2,column=0)
        
        self.Files.grid(row=0,column=0)
        self.Selection.grid(row=0,column=1)   
        self.ReadOptions.grid(row=1,column=0) 
        self.OtherOptions.grid(row=1,column=1)
        self.Run.grid(row=3,column=0)
        
    def check_sel(self):
        import MDAnalysis as MD
        U = MD.Universe(self.file_names['topol'].get(),self.file_names['traj'].get())
        rec = U.select_atoms(self.par['rec'].get())
        lig = U.select_atoms(self.par['lig'].get())
        self.rec_atoms.set("{0:d} Particles Selected in {1:d} residues".format(len(rec),len(rec.residues)))
        self.lig_atoms.set("{0:d} Particles Selected in {1:d} residues".format(len(lig),len(lig.residues)))
        return

    
    def run(self):
        phr_opt = {}
        phr_find_opt = {}
        
        for ft in self.files: 
            phr_opt[ft] = self.file_names[ft].get()
        
        for par in ['rec','lig','cutoff','group']:
            phr_opt[par] = self.par[par].get() 
        
        do_parallel = self.opt['do_parallel'].get()

        self.P = phrases.phrases(**phr_opt)

        if do_parallel==True:
            for par in ['s','group','group_num']:
                phr_find_opt[par] = self.par[par].get()
            self.start()
            self.P.find_phrases_parallel(**phr_find_opt)
        else:
            for par in ['b','e','s','group','group_num']:
                phr_find_opt[par] = self.par[par].get()
            self.P.find_phrases(**phr_find_opt)
        
        self.P.calc_dist()
        self.P.save("{0:s}.dat".format(self.file_out.get()))
        numpy.savez("{0:s}-distance".format(self.file_out.get()),P.D)
        return

if __name__ == "__main__":
    root = tk.Tk()
    root.title('Read Traj GUI')
    Main = ReadTrajGUI(root)
    root.mainloop()
    
    exit()
