#!/usr/bin/env python
# -*- coding: utf-8 -*-

import phrases
import numpy 
import sys, os

if sys.version_info[0] < 3:
    print ("This version is only compatible with Python 3.*")
    exit()
else:
    import tkinter as tk
    from tkinter import ttk
    from tkinter import filedialog
    from tkinter import messagebox
    

LOGO_FILE=os.path.dirname(phrases.__file__)+'/Logo.gif'
px=5
py=5

class LabelInput(tk.Frame):
    def __init__(self, parent, label='', input_class=ttk.Entry, input_var=None, input_args=None, label_args=None, input_list=None, update_val=None, **kwargs):
        super(LabelInput,self).__init__(parent, **kwargs)
        input_args = input_args or {}
        label_args = label_args or {}
        input_list = input_list or []
        if input_class in (ttk.Checkbutton, ttk.Button, ttk.Radiobutton):
            input_args['text'] = label
            input_args['variable'] = input_var
        elif input_class in (ttk.Scale,tk.Scale):
            self.label = ttk.Label(self, text=label, **label_args)
            self.label.grid(row=0, column=0, sticky=(tk.W + tk.E))
            input_args['variable'] = input_var
            input_args['orient'] = tk.HORIZONTAL
        else:
            self.label = ttk.Label(self, text=label, **label_args)
            self.label.grid(row=0, column=0, sticky=(tk.W + tk.E))
            input_args['textvariable'] = input_var
        self.input = input_class(self, *input_list, **input_args)
        self.input.grid(row=1, column=0)
        if update_val != None:
            self.update = ttk.Label(self, textvariable = update_val)
            self.update.grid(row=2,column=0, sticky=(tk.W + tk.E))
        #self.columnconfigure(0,weight=1)
        
    def grid(self, sticky=(tk.W + tk.E), **kwargs):
        super(LabelInput,self).grid(sticky=sticky,padx=px,pady=py,**kwargs)
    
    def get(self):
        try:
            if self.variable:
                return self.variable.get()
            elif type(self.input) == tk.Text:
                return self.input.get('0.0',tk.END)
            else:
                return self.input.get()
        except:
            return ''
    
    def set(self, value, *args, **kwargs):
        if type(self.variable) == tk.BooleanVar:
            self.variable.set(bool(value))
        elif self.variable:
            self.variable.set(value, *args, **kwargs)
        elif type(self.input) in (ttk.Checkbutton,
            ttk.Radiobutton):
            if value:
                self.input.select()
            else:
                self.input.deselect()
        elif type(self.input) == tk.Text:
            self.input.delete('1.0', tk.END)
            self.input.insert('1.0', value)
        else: # input must be an Entry-type widget with no variable
            self.input.delete(0, tk.END)
            self.input.insert(0, value)
    
    
class SelectFileButton(tk.Frame):
    def __init__(self, parent, label='', label_args=None, file_name=None, file_type=None, **kwargs):
        super(SelectFileButton,self).__init__(parent, **kwargs)
        self.label_args = label_args or {}
        self.file_name = file_name or tk.StringVar()
        self.file_type = file_type or [('All files', '*')]
        self.ft = label
        self.file_basename = tk.StringVar()
        self.file_basename.set(os.path.basename(self.file_name.get()))
        self.label  = ttk.Label(self, textvariable=self.file_basename, **self.label_args)
        self.button = ttk.Button(self,text=label,command=self._open_file_menu(self.ft))
        self.label.grid(row=0, column=0, sticky=(tk.W + tk.E))
        self.button.grid(row=1, column=0, sticky=(tk.W + tk.E))
        #self.columnconfigure(0,weight=1)
    
    def grid(self, sticky=tk.NW, **kwargs):
        super(SelectFileButton,self).grid(sticky=sticky,**kwargs)
    
    def get(self):
        return self.file_name
    
    def _open_file_menu(self,ft):
        def f():
            self.file_name.set(filedialog.askopenfilename(title="{0:s} file".format(ft),filetypes = self.file_type))
            self.file_basename.set(os.path.basename(self.file_name.get()))
        return f

class ReadTrajGUI():
    def __init__(self, master, *args, **kwargs):
        
        self.master = master
        self.file_types = { 'traj' :  [('XTC', '*.xtc'), ('All files', '*')],
                            'topol':  [('TPR', '*.tpr'), ('All files', '*')]}
        self.file_names     = { ft : tk.StringVar() for ft in self.file_types }
        self.file_basenames = { ft : tk.StringVar() for ft in self.file_types }
        self.file_out  = tk.StringVar()
        
        self.par       = { 'b'         : tk.IntVar(),
                           'e'         : tk.IntVar(),
                           's'         : tk.IntVar(),
                           'group'     : tk.StringVar() ,
                           'group_num' : tk.IntVar() ,
                           'cutoff'    : tk.DoubleVar() , 
                           'rec'       : tk.StringVar() , 
                           'lig'       : tk.StringVar() }
                        
        self.opt       = { 'do_parallel' : tk.BooleanVar() }
        
        self.group_opt = [ "res", "all", "atom", "num_atoms" ]
        
        self.inputs = {}
        
        self.Files        = tk.LabelFrame(self.master, text = "Files")
        self.Selection    = tk.LabelFrame(self.master, text = "Selection")
        self.ReadOptions  = tk.LabelFrame(self.master, text = "Read Options")
        self.OtherOptions = tk.LabelFrame(self.master, text = "Other Options")
        self.Run          = ttk.Button(self.master, text="Run", command = self.run )
        self.ToLinkage    = ttk.Button(self.master, text="To Linkage", command = self.run_linkage, state='disabled' )

        #Files Form
        for n,ft in enumerate(self.file_types):
            self.inputs[ft] = SelectFileButton(self.Files, label=ft, file_name = self.file_names[ft], file_type = self.file_types[ft])
            self.inputs[ft].grid(row=n,column=0)
        
        self.inputs['output'] = LabelInput(self.Files,label='Output',input_var=self.file_out)
        self.inputs['output'].grid(row=n+1,column=0)
        
        #Selection Options
        self.rec_atoms = tk.StringVar()
        self.lig_atoms = tk.StringVar()
        self.inputs['rec']       = LabelInput(self.Selection, label='receptor', input_var=self.par['rec'], update_val = self.rec_atoms )
        self.inputs['lig']       = LabelInput(self.Selection, label='ligand',   input_var=self.par['lig'], update_val = self.lig_atoms )
        self.inputs['check_sel'] = LabelInput(self.Selection, label='Check Selections', input_class = ttk.Button, input_args={ "command": self.check_sel } )
        
        self.inputs['rec'].grid(row=0,column=0) 
        self.inputs['lig'].grid(row=1,column=0)   
        self.inputs['check_sel'].grid(row=2,column=0)
        
        #Reading Option
        self.inputs['b']           = LabelInput(self.ReadOptions, label='begin', input_var=self.par['b'])
        self.inputs['e']           = LabelInput(self.ReadOptions, label='end', input_var=self.par['e'])
        self.inputs['s']           = LabelInput(self.ReadOptions, label='skip', input_var=self.par['s'])
        self.inputs['do_parallel'] = LabelInput(self.ReadOptions, input_class = ttk.Checkbutton, label="Parallel Routines", input_var = self.opt['do_parallel'])
        
        
        self.inputs['b'].grid(row=0,column=0)      
        self.inputs['e'].grid(row=1,column=0)        
        self.inputs['s'].grid(row=2,column=0)       
        self.inputs['do_parallel'].grid(row=3,column=0)
        
        #Other Options
        self.inputs['cutoff']    = LabelInput(self.OtherOptions, label='Distance cut-off [Å]', input_var = self.par['cutoff'])
        self.inputs['group']     = LabelInput(self.OtherOptions, input_class = ttk.Combobox, label='Grouping Method', input_var = self.par['group'], input_args ={ 'values': self.group_opt } )
        self.inputs['group_num'] = LabelInput(self.OtherOptions, label='Grouping number', input_var = self.par['group_num'])
        
        self.inputs['group_num'].input.config(state='disabled')
        self.inputs['group'].input.bind("<<ComboboxSelected>>",func=self.activate_groupnum)
        
        self.inputs['cutoff'].grid(row=0,column=0)
        self.inputs['group'].grid(row=1,column=0)
        self.inputs['group_num'].grid(row=2,column=0)
        
        #Default Values
        self.par["b"].set(1)
        self.par["e"].set(-1)
        self.par["s"].set(1)
        self.par["group"].set("res")
        self.par["group_num"].set(1)
        self.par["cutoff"].set(7.0)
        
        self.Files.grid(row=0,column=0,padx=px,pady=py)
        self.Selection.grid(row=0,column=1,padx=px,pady=py)   
        self.ReadOptions.grid(row=1,column=0,padx=px,pady=py) 
        self.OtherOptions.grid(row=1,column=1,padx=px,pady=py)
        self.Run.grid(row=3,column=0,padx=px,pady=py)
        self.ToLinkage.grid(row=3,column=1,padx=px,pady=py)
        
    def toggle_parallel(self):
        if self.opt['do_parallel'].get() == False:
            self.inputs['b'].input.config(state='active')
            self.inputs['e'].input.config(state='active')
        else:
            self.inputs['b'].input.config(state='disabled')
            self.inputs['e'].input.config(state='disabled')
            self.par['b'].set(0)
            self.par['e'].set(-1)
        return
    
    def activate_groupnum(self,event):
        if self.par['group'].get() == 'num_atoms':
            self.inputs['group_num'].input.config(state='active')
        else:
            self.inputs['group_num'].input.config(state='disabled')
        return
    
    def check_sel(self):
        self.ToLinkage.config(state='disabled')
        import MDAnalysis as MD
        U = MD.Universe(self.file_names['topol'].get(),self.file_names['traj'].get())
        rec = U.select_atoms(self.par['rec'].get())
        lig = U.select_atoms(self.par['lig'].get())
        self.rec_atoms.set("{0:d} Particles Selected in {1:d} residues".format(len(rec),len(rec.residues)))
        self.lig_atoms.set("{0:d} Particles Selected in {1:d} residues".format(len(lig),len(lig.residues)))
        
        return

    
    def run(self):
        phr_opt = {}
        phr_find_opt = {}
        self.ToLinkage.config(state='disabled')
        for ft in self.file_types: 
            phr_opt[ft] = self.file_names[ft].get()
        
        for p in ['rec','lig','cutoff','group']:
            phr_opt[p] = self.par[p].get()
        
        do_parallel = self.opt['do_parallel'].get()

        self.P = phrases.phrases(**phr_opt)

        if do_parallel==True:
            for p in ['s','group','group_num']:
                phr_find_opt[p] = self.par[p].get()
            self.P.find_phrases_parallel(**phr_find_opt)
        else:
            for p in ['b','e','s','group','group_num']:
                phr_find_opt[p] = self.par[p].get()
            self.P.find_phrases(**phr_find_opt)
        
        self.P.calc_dist()
        self.P.save("{0:s}.dat".format(self.file_out.get()))
        numpy.savez("{0:s}-distance".format(self.file_out.get()),self.P.D)
        self.ToLinkage.config(state='active')
        return
    
    def run_linkage(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.title('Linkage')
        rx,ry = self.master.winfo_x(), self.master.winfo_y()
        rw,rh = self.master.winfo_width(), self.master.winfo_height()
        self.newWindow.resizable(width=False,height=False)
        phf="{0:s}.dat".format(self.file_out.get())
        phd="{0:s}-distance.npz".format(self.file_out.get())
        self.app = LinkageGUI(self.newWindow,phrases_file=phf,dist=phd,b=self.par['b'].get(),e=self.par['e'].get(),s=self.par['s'].get(),dist_cutoff=self.par['cutoff'].get())
        return
        
class LinkageGUI():
    def __init__(self, master, *args, **kwargs):
        
        self.master = master
        self.file_types = { 'phrases_file' :  [('PHRASES', '*.dat'), ('All files', '*')],
                            'dist'    :  [('NUMPY', '*.npz'), ('All files', '*')]}
        self.file_names     = { ft : tk.StringVar() for ft in self.file_types }
        
        
        self.file_basenames = { ft : tk.StringVar() for ft in self.file_types }
        self.file_out  = tk.StringVar()
        
        self.par       = { 'res0'        : tk.IntVar(),
                           'b'           : tk.IntVar(),
                           'e'           : tk.IntVar(),
                           's'           : tk.IntVar(),
                           'dist_cutoff' : tk.DoubleVar(),
                           'cutoff'      : tk.DoubleVar(), 
                           'threshold'   : tk.DoubleVar()}
        
        
                
        self.opt       = { 'do_parallel' : tk.BooleanVar(), 
                           'calc_occ'    : tk.BooleanVar()}
        
        self.inputs = {}
        
        self.Files        = tk.LabelFrame(self.master, text = "Files")
        self.Parameters   = tk.LabelFrame(self.master, text = "Parameters")
        self.Options      = tk.LabelFrame(self.master, text = "Options")
        self.Run          = ttk.Button(self.master, text="Run", command = self.run )
        self.Print        = ttk.Button(self.master, text='Print Clusters', command = self.print_dendrogram )
        self.IPlot        = ttk.Button(self.master, text='Interactiv Clusters Plot', command = self.interactive_dendrogram )
        
        for k in kwargs:
            if k in self.file_names:
                print("Importing {0:s} = {1:s}".format(k,str(kwargs[k])))
                #self.inputs[k].file_name.set(kwargs[k])
                self.file_names[k].set(kwargs[k])
                #self.inputs[k].file_basename.set(os.path.basename(self.file_names[k].get()))
                self.file_basenames[k].set(os.path.basename(self.file_names[k].get()))
        
        #Files Form
        for n,ft in enumerate(self.file_types):
            self.inputs[ft] = SelectFileButton(self.Files, label=ft, file_name = self.file_names[ft], file_type = self.file_types[ft])
            self.inputs[ft].grid(row=n,column=0)
        self.inputs['output'] = LabelInput(self.Files,label='Output',input_var=self.file_out)
        self.inputs['output'].grid(row=n+1,column=0)
        
        
        
        #Parameters
        
        self.inputs['res0']        = LabelInput(self.Parameters, label="Residue '0'", input_var=self.par['res0'] )
        self.inputs['b']           = LabelInput(self.Parameters, label="Begin", input_var=self.par['b'] )
        self.inputs['e']           = LabelInput(self.Parameters, label="End", input_var=self.par['e'] )
        self.inputs['s']           = LabelInput(self.Parameters, label="Skip", input_var=self.par['s'] )
        self.inputs['dist_cutoff'] = LabelInput(self.Parameters, label="Distance cut-off [Å]", input_var=self.par['dist_cutoff'] )
        self.inputs['cutoff']      = LabelInput(self.Parameters, label="Clustering cut-off [-log10(P)]", input_var=self.par['cutoff'] )
        self.inputs['threshold']   = LabelInput(self.Parameters, label="Threshold [-log10(P)]", input_var=self.par['threshold'] )
        self.inputs['upd_par']     = LabelInput(self.Parameters, label='Update Parameters', input_class = ttk.Button, input_args={ "command": self.upd_par } )
        
        for n,p in enumerate(self.par):
            self.inputs[p].grid(row=n,column=0)
        self.inputs['upd_par'].grid(row=n+1, column=0)
        
        #Options
        self.inputs['calc_occ'] = LabelInput(self.Options, input_class = ttk.Checkbutton, label="Calculate Occupancy", input_var = self.opt['calc_occ'])
        self.inputs['do_parallel'] = LabelInput(self.Options, input_class = ttk.Checkbutton, label="Parallel Routines", input_var = self.opt['do_parallel'])
        
        self.inputs['calc_occ'].grid(row=0,column=0)
        self.inputs['do_parallel'].grid(row=1,column=0)
        
        
        self.par["res0"].set(1)
        self.par["b"].set(1)
        self.par["e"].set(-1)
        self.par["s"].set(1)
        self.par["dist_cutoff"].set(7.0)
        self.par["cutoff"].set(1.0)
        self.par["threshold"].set(-1.0)
        
        for k in kwargs:
            if k in self.par:
                print("Importing {0:s} = {1:s}".format(k,str(kwargs[k])))
                self.par[k].set(kwargs[k])
        
        self.test = False
        
        self.Files.grid(row=0,column=0,padx=px,pady=py)
        self.Parameters.grid(row=0,column=1,padx=px,pady=py)   
        self.Options.grid(row=1,column=0,padx=px,pady=py) 
        self.Run.grid(row=3,column=0,padx=px,pady=py)
        self.Print.grid(row=3,column=1,padx=px,pady=py)
        self.IPlot.grid(row=4,column=1,padx=px,pady=py)
    
    def upd_par(self):
        ph_opt     = { ft:self.file_names[ft].get() for ft in self.file_types }
        self.P = phrases.read_phrases(**ph_opt)
        self.test = True
        self.par['dist_cutoff'].set(self.P.cutoff)
        self.par['cutoff'].set(self.P.best_cutoff())
        return
        
    def run(self):
                
        base_name   = self.file_out.get()
        ph_opt     = { ft:self.file_names[ft].get() for ft in self.file_types }
        
        b           = self.par['b'].get()
        e           = self.par['e'].get()
        s           = self.par['s'].get()
        res0        = self.par['res0'].get()
        dist_cutoff = self.par['dist_cutoff'].get()
        cutoff      = self.par['cutoff'].get()
        threshold   = self.par['threshold'].get()
        
        occupancy   = self.opt['calc_occ'].get()
        do_parallel = self.opt['do_parallel'].get()
        
        if self.test==False:
            self.P = phrases.read_phrases(**ph_opt)
        
        self.test = True
        
        if (cutoff <= 0.0) :
            cutoff = self.P.best_cutoff()
        
        if (threshold <= 0.0) :
            threshold = None
        
        if occupancy:
            if do_parallel:
                if threshold == None:
                    self.P.calc_occupancy_parallel(b=b,e=e,s=s,Dcf=cutoff)
                else:
                    self.P.calc_occupancy_parallel(b=b,e=e,s=s,Dcf=cutoff,Dtsh=threshold)
            else:
                if threshold == None:
                    self.P.calc_occupancy(b=b,e=e,s=s,Dcf=cutoff)
                else:
                    self.P.calc_occupancy(b=b,e=e,s=s,Dcf=cutoff,Dtsh=threshold)
            
            self.P.calc_rate(dist_cutoff=dist_cutoff)
            self.P.save_occupancy('{0:s}-occupancy'.format(base_name))
            self.P.save_ACF('{0:s}-ACF'.format(base_name))
            self.P.as_dataframe(file_name='{0:s}-rate'.format(base_name))
            self.P.save_receptor_structure('{0:s}-receptor.pdb'.format(base_name))
            #with open('{0:s}-rates.txt'.format(base_name),'w') as f_out:
            #    f_out.write('{0:s}'.format(self.P))
        else:
            
            self.P.find_cluster(Dtsh=threshold)
            self.P.save_receptor_structure('{0:s}-receptor.pdb'.format(base_name))
        print("Done!")
        return
        
    def print_dendrogram(self,filename=None):
        newWindow = tk.Toplevel(self.master)
        newWindow.title('Dendrogram Plot')
        app = FixedPlot(newWindow,self.P,self.par,file_out=self.file_out)
        return
    
    def interactive_dendrogram(self):
        newWindow = tk.Toplevel(self.master)
        newWindow.title('Interactive Dendrogram Plot')
        app = InteractivePlot(newWindow,self.P,self.par)
        return
    
class FixedPlot():
    def __init__(self,master,P,par,file_out=None):
        import matplotlib.backends.backend_tkagg as tkFigure
        import matplotlib.pyplot as plt
        self.dpi = 96
        
        self.master = master
        self.P = P
        
        self.file_types = { 'image_file' :  [('SVG', '*.svg'), ('PDF', '*.pdf'), ('PNG', '*.png'), ('All files', '*')],}
        self.file_names     = { ft : tk.StringVar() for ft in self.file_types }        
        self.file_basenames = { ft : tk.StringVar() for ft in self.file_types }
        self.file_names['image_file'].set(file_out.get()+'.pdf')
        
        self.par       = { 'res0'        : tk.IntVar(),
                           'cutoff'      : tk.DoubleVar(), 
                           'threshold'   : tk.DoubleVar()}
        self.inputs    = {}
        
        
        for k in self.par:
            self.par[k].set(par[k].get())
        
        self.Canvas   = tk.LabelFrame(self.master,text='Dendrogram')
        self.Files    = tk.LabelFrame(self.master,text='Save to File')
        
        self.inputs['image_file'] = SelectFileButton(self.Files, label='image_file', file_name = self.file_names['image_file'], file_type = self.file_types['image_file'])
        self.inputs['image_file'].grid(row=0,column=0)
        self.Save = ttk.Button(self.Files, text="Save", command = self.savefig )
        self.Save.grid(row=2,column=0)
        
        self.figure      = plt.figure(figsize=(8,8),dpi=self.dpi)
        self.plot_canvas = tkFigure.FigureCanvas(self.figure,master=self.Canvas)
        
        self.dendrogram()
        self.plot_canvas.draw()
        self.plot_canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
        
        self.Canvas.grid(row=0,column=0)
        self.Files.grid(row=1,column=0)

    def dendrogram(self):
        import matplotlib.pyplot as plt
        import scipy.cluster.hierarchy as sch
        
        res0 = self.par['res0'].get()
        Dcf  = self.par['cutoff'].get()
        Dtsh = self.par['threshold'].get()
        
        
        if numpy.all(self.P.D == None):
            self.P.calc_dist()
        subs=numpy.where(numpy.diag(self.P.D)< Dtsh)[0]
        if len(subs) < 1:
            print("Threshold ({0:8.3f}) Corresponds to 0 residues".format(Dtsh))
            return
        res=numpy.array([ l.resname+str(l.resid+res0) for l in self.P.receptor.residues])
        
        d_s=self.P.D[subs][:,subs]
        res_s=res[subs]
        idx = numpy.triu_indices(d_s.shape[0],1)
        # perform two clustering to create two independent copies
        Y1 = sch.linkage(d_s[idx], method='complete')
        Y2 = sch.linkage(d_s[idx], method='complete')
        
        #Create the Canvas
        fig = self.figure
        
        fs = min(1.0,self.dpi*0.9/(2*len(subs))) # font size rescale
        
        # Right side Dendrogram
        ax1 = fig.add_axes([0.8,0.1,0.15,0.6])
        Z1 = sch.dendrogram(Y1, color_threshold=Dcf, orientation='right')
        idx1 = Z1['leaves']
        ax1.set_xticks([])
        ax1.set_yticklabels(res_s[idx1],size=int(fs*fig.get_size_inches()[1]))
        
        # Top side Dendrogram
        ax2 = fig.add_axes([0.09,0.8,0.6,0.15])
        Z2 = sch.dendrogram(Y2, color_threshold=Dcf)
        idx2 = Z2['leaves']
        #ax2.set_xticks([])
        ax2.set_xticklabels(res_s[idx2],size=int(fs*fig.get_size_inches()[1]))
        ax2.plot(ax2.get_xlim(),[Dcf,Dcf],'--',lw=0.25) #draw horizontal line at cutoff
        
        # Reordered Large Distance Matrix
        axmatrix = fig.add_axes([0.09,0.1,0.6,0.6])
        im = axmatrix.matshow(d_s[idx1][:,idx2], aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        im.set_clim(numpy.min(self.P.D),numpy.max(self.P.D))
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        
        # Original Distance Matrix (Small)
        axmatrix2 = fig.add_axes([0.74,0.74,0.18,0.18])
        im = axmatrix2.matshow(self.P.D, aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        
        # Colorbar
        cbaxes = fig.add_axes([0.95,0.1, 0.01, 0.6])
        plt.colorbar(im, cax=cbaxes)
        return
        
    def savefig(self):
        import matplotlib.pyplot as plt
        if self.file_names['image_file'].get() == None:
            print("Insert Output FileName in the form !")
            return
        else:
            plt.savefig(self.file_names['image_file'].get())
            return

class InteractivePlot():
    def __init__(self,master,P,par):
        import matplotlib.backends.backend_tkagg as tkFigure
        import matplotlib.pyplot as plt
        self.dpi = 96
        
        self.master = master
        self.P = P
        
        
        self.par       = { 'res0'        : tk.IntVar(),
                           'cutoff'      : tk.DoubleVar(), 
                           'threshold'   : tk.DoubleVar()}
        self.inputs    = {}
        
        for k in self.par:
            self.par[k].set(par[k].get())
        
        self.Canvas     = tk.LabelFrame(self.master,text='Dendrogram')
        self.Parameters = tk.LabelFrame(self.master,text='Parameters')
        
        
        self.inputs['cutoff'] = LabelInput(self.Parameters, input_class = tk.Scale, label='cutoff', input_var=self.par['cutoff'], input_args = {'from': 0.0, 'to': numpy.max(self.P.D), 'command': self.upd_plot} )
        self.inputs['threshold'] = LabelInput(self.Parameters, input_class = tk.Scale, label='threshold', input_var=self.par['threshold'], input_args = {'from': numpy.min(self.P.D), 'to': numpy.max(self.P.D), 'command': self.upd_plot} )
        
        self.inputs['cutoff'].input.set(self.par['cutoff'].get())
        self.inputs['threshold'].input.set(numpy.max(self.P.D)) 
        
        self.inputs['threshold'].input.config(tickinterval=0.5,resolution=0.1,state='active')
        self.inputs['threshold'].input.config(tickinterval=0.5,resolution=0.1,state='active')
        self.inputs['cutoff'].grid(row=0,column=0)
        self.inputs['threshold'].grid(row=1,column=0)
        
        self.figure      = plt.figure(figsize=(8,8),dpi=self.dpi)
        self.plot_canvas = tkFigure.FigureCanvas(self.figure,master=self.Canvas)
        
        self.dendrogram()
        self.plot_canvas.draw()
        self.plot_canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
        
        self.Canvas.grid(row=0,column=0)
        self.Parameters.grid(row=1,column=0)
        

    def dendrogram(self):
        
        import matplotlib.pyplot as plt
        import scipy.cluster.hierarchy as sch
        
        res0 = self.par['res0'].get()
        Dcf  = self.par['cutoff'].get()
        Dtsh = self.par['threshold'].get()
        
        
        if numpy.all(self.P.D == None):
            self.P.calc_dist()
        subs=numpy.where(numpy.diag(self.P.D)< Dtsh)[0]
        if len(subs) < 1:
            print("Threshold ({0:8.3f}) Corresponds to 0 residues".format(Dtsh))
            return
        res=numpy.array([ l.resname+str(l.resid+res0) for l in self.P.receptor.residues])
        
        d_s=self.P.D[subs][:,subs]
        res_s=res[subs]
        idx = numpy.triu_indices(d_s.shape[0],1)
        # perform two clustering to create two independent copies
        Y1 = sch.linkage(d_s[idx], method='complete')
        Y2 = sch.linkage(d_s[idx], method='complete')
        
        #Create the Canvas
        fig = self.figure
        
        fs = min(1.0,self.dpi*0.9/(2*len(subs))) # font size rescale
        
        # Right side Dendrogram
        ax1 = fig.add_axes([0.8,0.1,0.15,0.6])
        Z1 = sch.dendrogram(Y1, color_threshold=Dcf, orientation='right')
        idx1 = Z1['leaves']
        ax1.set_xticks([])
        ax1.set_yticklabels(res_s[idx1],size=int(fs*fig.get_size_inches()[1]))
        
        # Top side Dendrogram
        ax2 = fig.add_axes([0.09,0.8,0.6,0.15])
        Z2 = sch.dendrogram(Y2, color_threshold=Dcf)
        idx2 = Z2['leaves']
        #ax2.set_xticks([])
        ax2.set_xticklabels(res_s[idx2],size=int(fs*fig.get_size_inches()[1]))
        ax2.plot(ax2.get_xlim(),[Dcf,Dcf],'--',lw=0.25) #draw horizontal line at cutoff
        
        # Reordered Large Distance Matrix
        axmatrix = fig.add_axes([0.09,0.1,0.6,0.6])
        im = axmatrix.matshow(d_s[idx1][:,idx2], aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        im.set_clim(numpy.min(self.P.D),numpy.max(self.P.D))
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        
        # Original Distance Matrix (Small)
        axmatrix2 = fig.add_axes([0.74,0.74,0.18,0.18])
        im = axmatrix2.matshow(self.P.D, aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        
        # Colorbar
        cbaxes = fig.add_axes([0.95,0.1, 0.01, 0.6])
        plt.colorbar(im, cax=cbaxes)
        
        return
    
    def upd_plot(self,val):
        #def dendrogram(self,figure,res0=0,Dcf=None,Dtsh=None):
        
        import matplotlib.pyplot as plt
        import scipy.cluster.hierarchy as sch
        
        res0 = self.par['res0'].get()
        Dcf  = self.par['cutoff'].get()
        Dtsh = self.par['threshold'].get()
        dpi = 96 
        
        subs=numpy.where(numpy.diag(self.P.D)< Dtsh)[0]
        res=numpy.array([ l.resname+str(l.resid+res0) for l in self.P.receptor.residues])
        
        d_s=self.P.D[subs][:,subs]
        res_s=res[subs]
        idx = numpy.triu_indices(d_s.shape[0],1)
        # perform two clustering to create two independent copies
        Y1 = sch.linkage(d_s[idx], method='complete')
        Y2 = sch.linkage(d_s[idx], method='complete')
        
        #Create the Canvas
        fig = self.figure
        
        fs = min(1.0,dpi*0.9/(2*len(subs))) # font size rescale
        
        
        ax1       = fig.axes[0]# Right side Dendrogram
        ax2       = fig.axes[1]# Top side Dendrogram
        axmatrix  = fig.axes[2]# Matrix
        axmatrix2 = fig.axes[3]
        cbaxes    = fig.axes[4]
        
        Z1 = sch.dendrogram(Y1, color_threshold=Dcf, orientation='right',ax=ax1)
        idx1 = Z1['leaves']
        ax1.set_xticks([])
        ax1.set_yticklabels(res_s[idx1],size=int(fs*fig.get_size_inches()[1]))
        
        
        Z2 = sch.dendrogram(Y2, color_threshold=Dcf,ax=ax2)
        idx2 = Z2['leaves']
        ax2.set_xticklabels(res_s[idx2],size=int(fs*fig.get_size_inches()[1]))
        ax2.plot(ax2.get_xlim(),[Dcf,Dcf],'--',lw=0.25) #draw horizontal line at cutoff
        
        # Reordered Large Distance Matrix
        
        im = axmatrix.matshow(d_s[idx1][:,idx2], aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        im.set_clim(numpy.min(self.P.D),numpy.max(self.P.D))
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        
        # Original Distance Matrix (Small)
        
        #im = axmatrix2.matshow(self.P.D, aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        
        # Colorbar
        #plt.colorbar(im, cax=cbaxes)
        
        self.plot_canvas.draw()
        
        return
        
class MainGUI():
    def __init__(self,master):
        self.master      = master
        self.frame       = ttk.Frame(self.master,width='6c')
        self.logo_image  = tk.PhotoImage(file=LOGO_FILE)
        self.logo        = tk.Canvas(self.frame,width='6c',height='4c')
        self.button1     = ttk.Button(self.frame, text = 'Read-Traj', width = 25, command = self.open_read)
        self.button2     = ttk.Button(self.frame, text = 'Linkage'  , width = 25, command = self.open_linkage)
        self.exit_button = ttk.Button(self.frame, text = 'Exit'  , width = 12, command = self.exit_gui)
        
        self.logo.create_image((0,0),image=self.logo_image,anchor=tk.NW)
        
        self.frame.pack()
        self.logo.pack()
        self.button1.pack()
        self.button2.pack()
        self.exit_button.pack()
        

    def open_read(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.title('Read traj')
        rx,ry = self.master.winfo_x(), self.master.winfo_y()
        rw,rh = self.master.winfo_width(), self.master.winfo_height()
        self.newWindow.resizable(width=False,height=False)
        self.app = ReadTrajGUI(self.newWindow)
    
    def open_linkage(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.title('Linkage')
        rx,ry = self.master.winfo_x(), self.master.winfo_y()
        rw,rh = self.master.winfo_width(), self.master.winfo_height()
        self.newWindow.resizable(width=False,height=False)
        self.app = LinkageGUI(self.newWindow)
    
    def exit_gui(self):
        exit()

if __name__ == "__main__":
    root = tk.Tk()
    root.title('Phrases GUI')
    root.resizable(width=False,height=False)
    Main = MainGUI(root)
    root.mainloop()
    
    exit()
