#!/usr/bin/env python

import MDAnalysis as MD
from argparse import ArgumentParser
parser = ArgumentParser( description = 'Cationize GLU and ASP sidechains')
#
# INPUT FILES
#
parser.add_argument("-p","--pdb",dest="pdb",action="store",type=str,default=None,help="Input PROTEIN PDB",required=True,metavar="PDB FILE")
parser.add_argument("-l","--list",dest="list_res",action="store",type=str,default=None,help="File defining the list of GLU residues in the PROTEIN PDB to be modified",required=False,metavar="TXT FILE")
#
# OUTPUT FILES
#
parser.add_argument("-o","--out",dest="out",action="store",type=str,default=None,required=True,help="Output PDB",metavar="PDB FILE")
#
# VAR ARGUMENTS
#
#

options = parser.parse_args()

prot=options.pdb

prot_res = { "ASP":"ASD", "GLU":"GLD" }  

resname = [ i.split()[0] for i in open(options.list_res,'r').readlines() ]
resnum  = [ int(i.split()[1]) for i in open(options.list_res,'r').readlines() ]

p = MD.Universe(prot)

out_sel="( resid "

for r,n in zip(resname,resnum):
    if r in prot_res.keys():
        print ("Cationizing RES {0:s}:{1:d} -> {2:s}".format(r,n,prot_res[r]))
        if p.residues[n-1].resname == r:
            p.residues[n-1].resname = prot_res[r]
            out_sel = out_sel + "{0} ".format(n)
        else:
            print("Residue {0:d} is not {1} (but {2})".format(n,r,p.residues[n-1].resname))

out_sel = out_sel + " and not name OD2 OE2 ) or not resname ASD GLD"

print(out_sel)

out=p.select_atoms(out_sel)

out.atoms.write(options.out)

quit()



