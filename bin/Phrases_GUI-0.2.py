#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import phrases
import Tkinter as Tk
import ttk
import tkFileDialog
import numpy
LOGO_FILE=os.path.dirname(phrases.__file__)+'/Logo.gif'

class PhraseGUI:
    def _open_file_menu(self,t):
        def open():
            self.file_name[t].set(tkFileDialog.askopenfilename(title="{0:s} file".format(t),filetypes = self.files[t]))
        return open
    
    def SelectFileButton(self,parent,label,ft):
        frame = Tk.Frame(parent)
        frame.pack(side="top",padx=2,pady=2)
        w = Tk.Button(frame,text=label,command=self.file_open[ft])
        w.pack(side="left")
        l = Tk.Label(frame, textvariable=self.file_name[ft])
        l.pack(side="left")
        return frame
    
    def Checkbutton(self,parent,label,p):
        w = Tk.Checkbutton(parent,text=label,variable=self.opt[p])
        w.pack(side="top")
        return w
    
    def Button(self,parent,label,c):
        w = Tk.Button(parent,text=label,command=c)
        w.pack(side="top")
        return w
        
    def text_entry(self,parent,variable,label):
        frame = Tk.Frame(parent)
        frame.pack(side="top",padx=2,pady=2)
        l = Tk.Label(frame, text=label)
        l.pack(side="left")
        w = Tk.Entry(frame, textvariable=variable,width=8)
        w.pack(side="left",anchor='w')
        return w
    
    def text_entry_update(self,parent,variable,update,label):
        frame = Tk.Frame(parent)
        frame.pack(side="top",padx=2,pady=2)
        l = Tk.Label(frame, text=label)
        l.pack(side="left")
        w = Tk.Entry(frame, textvariable=variable,width=8)
        w.pack(side="left",anchor='w')
        u = Tk.Label(frame, textvariable=update)
        u.pack(side="left")
        return w
    
    def drop_menu_entry(self,parent,variable,options,label):
        frame = Tk.Frame(parent)
        frame.pack(side="top",padx=2,pady=2)
        l = Tk.Label(frame, text=label)
        l.pack(side="top")
        w = Tk.OptionMenu(frame, variable, *options)
        w.pack(side="top",anchor='w')
        return w

class ReadTrajGUI(PhraseGUI):
    program = True
    descr = "Read a trajectory, find and cluster binding phrases, basic statistics"
    def __init__(self,master):
        self.master = master
        
        self.files = { 'xtc':  [('XTC', '*.xtc'), ('All files', '*')],
                       'tpr':  [('TPR', '*.tpr'), ('All files', '*')]}
        
        self.group_opt = [ "res", "all", "atom", "num_atoms" ]
        
        self.file_name = { ft : Tk.StringVar() for ft in self.files }
        out = [ self.file_name[ft].set(None) for ft in self.file_name ]
        self.file_open = { ft : self._open_file_menu(ft) for ft in self.files }
        self.file_out  = Tk.StringVar()
        self.par       = { 'begin'   : Tk.IntVar(),
                           'end'     : Tk.IntVar(),
                           'skip'    : Tk.IntVar(),
                           'group'   : Tk.StringVar() ,
                           'grp_num' : Tk.IntVar() ,
                           'cut-off' : Tk.DoubleVar() , 
                           'rec_str' : Tk.StringVar() , 
                           'lig_str' : Tk.StringVar() }
                        
        self.opt       = { 'do_parallel' : Tk.BooleanVar() }
        
        
        F = Tk.Frame(self.master)
              
        F_TRJ = self.SelectFileButton(F,"Select Trajectory",'xtc')
        F_TPR = self.SelectFileButton(F,"Select Topology",'tpr')
        F_OUT = self.text_entry(F, self.file_out,"Output File")
        F_p   = self.Checkbutton(F,"use parallel routines",'do_parallel')
        F_b   = self.text_entry(F,self.par['begin'],'begin')
        F_e   = self.text_entry(F,self.par['end'],'end')
        F_s   = self.text_entry(F,self.par['skip'],'skip')
        EXEC  = self.Button(F,"Start!",self.run)
        
        P = Tk.Frame(self.master)
        
        self.rec_atoms = Tk.StringVar()
        self.lig_atoms = Tk.StringVar()
        
        P_rec = self.text_entry_update(P,self.par['rec_str'],self.rec_atoms,'Receptor')
        P_lig = self.text_entry_update(P,self.par['lig_str'],self.lig_atoms,'Ligand')
        CHECK = self.Button(P,"Check Selections",self.check_sel)
        P_cutoff = self.text_entry(P,self.par['cut-off'],"Distance cut-off [Å]") 
        P_group = self.drop_menu_entry(P,self.par['group'],self.group_opt, "Grouping Method")
        P_grp_num= self.text_entry(P,self.par['grp_num'],'Grouping number')
        
        self.par["begin"].set(1)
        self.par["end"].set(-1)
        self.par["skip"].set(1)
        self.par["group"].set("res")
        self.par["grp_num"].set(1)
        self.par["cut-off"].set(7.0)
        
        F.pack(side="left")
        P.pack(side="left")
        
    def check_sel(self):
        import MDAnalysis as MD
        U = MD.Universe(self.file_name['tpr'].get(),self.file_name['xtc'].get())
        rec = U.select_atoms(self.par['rec_str'].get())
        lig = U.select_atoms(self.par['lig_str'].get())
        self.rec_atoms.set("{0:d} Particles Selected in {1:d} residues".format(len(rec),len(rec.residues)))
        self.lig_atoms.set("{0:d} Particles Selected in {1:d} residues".format(len(lig),len(lig.residues)))
        
    def run(self):
        top             = self.file_name['tpr'].get()
        trj             = self.file_name['xtc'].get()
        
        b               = self.par['begin'].get()
        e               = self.par['end'].get()
        skip            = self.par['skip'].get()
        rec_str         = self.par['rec_str'].get()
        lig_str         = self.par['lig_str'].get()
        lig_dist_cutoff = self.par['cut-off'].get()
        grp             = self.par['group'].get()
        num_atom_group  = self.par['grp_num'].get()
        
        do_parallel     = self.opt['do_parallel'].get()
        
        min_phrase_len  = 3
        
        P = phrases.phrases(topol=top,traj=trj,rec=rec_str,lig=lig_str,cutoff=lig_dist_cutoff,min_len=min_phrase_len,ligand_grp=grp)
        
        if do_parallel == True:
            P.find_phrases_parallel(s=skip,group=grp,group_num=num_atom_group)
        else:
            P.find_phrases(b,e,skip,group=grp,group_num=um_atom_group)
        
        P.save("{0:s}.dat".format(self.file_out))
        P.calc_dist()
        numpy.savez("{0:s}-distance".format(self.file_out.get()),P.D)
        
class LinkageGUI(PhraseGUI):
    program = True
    descr = "Performe Hierarchical Clustering of Residues based on Phrases Analysis and/or filter the Phrases based on clustering"
    def __init__(self, master):
        self.master = master
        
        self.files = { 'dat':  [('PHRASES', '*.dat'), ('All files', '*')],
                       'npz':  [('NUMPY', '*.npz'), ('All files', '*')]}
        
        self.group_opt = [ "res", "all", "atom", "num_atoms" ]
        
        self.file_name = { ft : Tk.StringVar() for ft in self.files }
        out = [ self.file_name[ft].set(None) for ft in self.file_name ]
        self.file_open = { ft : self._open_file_menu(ft) for ft in self.files }
        self.file_out  = Tk.StringVar()
        self.par       = { 'res0'   : Tk.IntVar(),
                           'skip'   : Tk.IntVar(),
                           'Binding cut-off'   : Tk.DoubleVar() , 
                           'Cluster cut-off'   : Tk.DoubleVar() , 
                           'Cluster threshold' : Tk.DoubleVar() }
                           
        self.opt       = { 'do_parallel'       : Tk.BooleanVar(),
                           'occupancy'         : Tk.BooleanVar() }
        
        F = Tk.Frame(self.master)
        F_PHR = self.SelectFileButton(F,"Select Phrases",'dat')
        F_DST = self.SelectFileButton(F,"Select Distance",'npz')
        F_OUT = self.text_entry(F, self.file_out,"Output File")
        F_p   = self.Checkbutton(F,"use parallel routines",'do_parallel')
        F_occ = self.Checkbutton(F,"calculate occupancy",'occupancy')
        EXEC  = self.Button(F,"Start!",self.run)
        
        P = Tk.Frame(self.master)
        [ self.text_entry(P,self.par[par],par) for par in self.par ]
        
        self.par["skip"].set(1)
        self.par["Cluster cut-off"].set(0.0)
        self.par["Cluster threshold"].set(0.0)
        
        F.pack(side="left")
        P.pack(side="left")
    
    def run(self):
        base_name   = self.file_out.get()
        phrases     = self.file_name['dat'].get()
        dist        = self.file_name['npz'].get()
        
        skip        = self.par['skip'].get()
        res0        = self.par['res0'].get()
        dist_cutoff = self.par['Binding cut-off'].get()
        cutoff      = self.par['Cluster cut-off'].get()
        threshold   = self.par['Cluster threshold'].get()
        
        do_parallel = self.opt['do_parallel'].get()
        occupancy   = self.opt['occupancy'].get()
        
        P = phrases.read_phrases(phrases,dist=dist)
        
        if (cutoff <= 0.0) :
            cutoff = P.best_cutoff()
        
        if (threshold <= 0.0) :
            threshold = None
        
        if occupancy:
            if do_parallel:
                if threshold == None:
                    P.calc_occupancy_parallel(s=skip,cutoff=cutoff)
                else:
                    P.calc_occupancy_parallel(s=skip,cutoff=cutoff,threshold=threshold)
            else:
                if threshold == None:
                    P.calc_occupancy(s=skip,cutoff=cutoff)
                else:
                    P.calc_occupancy(s=skip,cutoff=cutoff,threshold=threshold)
            
            P.calc_rate(dist_cutoff=dist_cutoff)
            P.save_occupancy('{0:s}-occupancy'.format(base_name))
            P.save_ACF('{0:s}-ACF'.format(base_name))
            P.as_dataframe(file_name='{0:s}-rate'.format(base_name))
            P.save_receptor_structure('{0:s}-receptor.pdb'.format(base_name))
            with open('{0:s}-rates.txt'.format(base_name),'w') as f_out:
                f_out.write('{0:s}'.format(P))
        else:
            P.find_cluster(threshold=threshold)
            P.save_receptor_structure('{0:s}-receptor.pdb'.format(base_name))
            
        if threshold == None:
            P.print_dendrogram(res0=res0,cutoff=cutoff)
        else:
            P.print_dendrogram(res0=res0,cutoff=cutoff,threshold=threshold)
        
        
class MainGUI:
    def __init__(self,master):
        self.master = master
        self.frame = Tk.Frame(self.master)
        self.logo_image = Tk.PhotoImage(file=LOGO_FILE)
        self.logo = Tk.Canvas(self.frame,width='6c',height='4c')
        self.logo.create_image((0,0),image=self.logo_image,anchor=Tk.NW)
        self.button1 = Tk.Button(self.frame, text = 'Read-Traj', width = 25, command = self.open_read)
        self.button2 = Tk.Button(self.frame, text = 'Linkage'  , width = 25, command = self.open_linkage)
        self.exit_button = Tk.Button(self.frame, text = 'Exit'  , width = 12, command = self.exit_gui)
        self.frame.pack()
        self.logo.pack()
        self.button1.pack()
        self.button2.pack()
        self.exit_button.pack()
        

    def open_read(self):
        self.newWindow = Tk.Toplevel(self.master)
        self.app = ReadTrajGUI(self.newWindow)
    
    def open_linkage(self):
        self.newWindow = Tk.Toplevel(self.master)
        self.app = LinkageGUI(self.newWindow)
    
    def exit_gui(self):
        exit()
        
if __name__ == "__main__":
    
    root = Tk.Tk()
    root.title('Phrases GUI')
    Main = MainGUI(root)
    root.mainloop()
    
    exit()
