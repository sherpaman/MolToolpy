#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import pickle
import phrases
from scipy import stats
from math import *
import MDAnalysis as MD
from argparse import ArgumentParser
parser = ArgumentParser( description = 'Find Binding Sites in Receptor+Ligand MD simulation')
subparsers = parser.add_subparsers(help='sub-command help',dest='program')
#
# TRAJ-READ PROGRAM SUB-PARSER
parser_read = subparsers.add_parser('read-traj',description="Read a trajectory, find and cluster binding phrases, basic statistics")
#
# INPUT FILES
#
parser_read.add_argument("-f","--traj",dest="traj",action="store",type=str,default=None,help="Input Trajectory",required=True,metavar="TRAJ FILE")
parser_read.add_argument("-t","--top",dest="top",action="store",type=str,default=None,help="Input Topology",required=True,metavar="TOPOL FILE")
#
# OUTPUT FILES
#
parser_read.add_argument("-o","--out",dest="out",action="store",type=str,default=None,required=True,help="Output File Name",metavar="DAT FILE")
#
# VAR ARGUMENTS
#
parser_read.add_argument("-d","--dist",dest="dist_cutoff",action="store",type=float,default=7.0,help="Distance Cut-off for binding definition",metavar="DIST.[Angstrom]")
parser_read.add_argument("-b","--begin",dest="begin",action="store",type=int,default=0,help="First frame to read")
parser_read.add_argument("-e","--end",dest="end",action="store",type=int,default=-1,help="Last frame to read")
parser_read.add_argument("-s","--skip",dest="skip",action="store",type=int,default=1,help="number of frame to skip", metavar="INTEGER")
parser_read.add_argument("-r","--receptor",dest="receptor",action="store",type=str,default="protein",help="Selection string for the Receptor")
parser_read.add_argument("-l","--ligand",dest="ligand",action="store",type=str,default="not protein",help="Selection string for the Ligand")
parser_read.add_argument("-g","--ligand_grouping",dest="ligand_group",action="store",type=str,default="res",help="Grouping for the Ligand")
parser_read.add_argument("--num_atom_group",dest="num_atom_group",action="store",type=int,default=None,help="Size of groups : for \"num_atom\" grouping option")
parser_read.add_argument("--do_parallel",dest="do_parallel",action="store_true",help="Use mdreader to speed-up trajectory reading using parallel process")
#
# LINKAGE ANALYSIS PROGRAM
parser_linkage = subparsers.add_parser('Linkage',description="Perform Hierarchical Clustering of Residues based on Phrases Analysis and/or filter the Phrases based on clustering")
#
# INPUT FILES
#
parser_linkage.add_argument("-p","--phrases",dest="phrases",action="store",type=str,default=None,help="Input Phrases list",required=True,metavar="PICKLE FILE")
parser_linkage.add_argument("-d","--npz",dest="dist",action="store",type=str,default=None,help="Input Phrases Distance Matrix",required=False,metavar="NPY FILE")
#
# OUTPUT FILES
#
parser_linkage.add_argument("-o","--out",dest="out",action="store",type=str,default=None,required=True,help="Output File Name",metavar="DAT FILE")
#
# VAR ARGUMENTS
#
parser_linkage.add_argument("--dist",dest="dist_cutoff",action="store",type=float,default=None,help="Distance Cut-off for binding definition. If not specified, the value defined in the Input Phrases list file is used.",metavar="DIST.[Angstrom]")
parser_linkage.add_argument("-t","--threshold",dest="threshold",action="store",type=float,default=np.inf,help="Minimum Probability Threshold for Clustering",required=False,metavar="PSEUDO_DIST")
parser_linkage.add_argument("-c","--cut-off",dest="cutoff",action="store",type=float,default=None,help="Linkage Clustering Cut-Off",required=False,metavar="PSEUDO_DIST")
parser_linkage.add_argument("--res0",dest="res0",action="store",type=int,default=1,help="Add this to residue numbering of Protein")
parser_linkage.add_argument("--occupancy",dest="occupancy",action="store_true",help="Perform occupancy calculation")
parser_linkage.add_argument("--do_parallel",dest="do_parallel",action="store_true",help="Use mdreader to speed-up trajectory reading using parallel process")
#
#
options = parser.parse_args()

# LOCAL FUNCTION DEFINITION
def read_cluster_file(file_in,r0,nr):
    with open(file_in) as f:
        clusters = [[]]
        labels = np.zeros(nr)
        for i,r in enumerate(f.readlines()):
            loc_cluster = []
            for e in r.split():
                resnum = int(e)-int(r0)
                loc_cluster.append(int(e)-r0)
                labels[resnum] = i+1
            clusters.append(np.array(loc_cluster))
            clusters[0] = [ n for n,i in enumerate(labels) if i == 0 ]
    return labels, clusters

if options.program=='read-traj':
    top             = options.top
    trj             = options.traj
    b               = options.begin
    e               = options.end
    skip            = options.skip
    rec_str         = options.receptor
    lig_str         = options.ligand
    lig_dist_cutoff = options.dist_cutoff
    min_phrase_len  = 3
    
    #u = MD.Universe(top,*trj)
    
    #receptor = u.select_atoms(rec_str)
    #[ligand = u.select_atoms(lig_str)
    
    P = phrases.phrases(topol=top,traj=trj,rec=rec_str,lig=lig_str,cutoff=lig_dist_cutoff,min_len=min_phrase_len)
    
    if options.do_parallel == True:
        P.find_phrases_parallel(s=skip,group=options.ligand_group,group_num=options.num_atom_group)
    else:
        P.find_phrases(b,e,skip,group=options.ligand_group,group_num=options.num_atom_group)
    
    P.save("{0:s}.dat".format(options.out))
    P.calc_dist()
    np.savez("{0:s}-distance".format(options.out),P.D)
    quit()

elif options.program == "Linkage":
    import scipy.cluster.hierarchy as sch
    base_name = options.out
    dist      = options.dist
    res0      = options.res0
    
    P = phrases.read_phrases(options.phrases,dist=dist)
    
    if options.cutoff == None:
        cutoff = P.best_cutoff()
    else:
        cutoff = options.cutoff
    
    if options.occupancy == True:

        if options.do_parallel == True:
            P.calc_occupancy_parallel(Dcf=cutoff,Dtsh=options.threshold)
        else:
            P.calc_occupancy(Dcf=cutoff,Dtsh=options.threshold)

        P.calc_rate(dist_cutoff=options.dist_cutoff)
        P.save_occupancy('{0:s}-occupancy'.format(base_name))

        if options.threshold == None:
            P.save_receptor_structure('{0:s}_{2:3.1f}-receptor.pdb'.format(base_name,cutoff),vmd_script='{0:s}_{1:3.1f}-{2:3.1f}-receptor.vmd'.format(base_name,cutoff),min_rate=5.0)
        else:
            P.save_receptor_structure('{0:s}_{1:3.1f}-{2:3.1f}-receptor.pdb'.format(base_name,options.threshold,cutoff),vmd_script='{0:s}_{1:3.1f}-{2:3.1f}-receptor.vmd'.format(base_name,options.threshold,cutoff),min_rate=5.0)

    else:
        P.find_cluster(Dtsh=options.threshold)

        if options.threshold == None:
            P.save_receptor_structure('{0:s}_{2:3.1f}-receptor.pdb'.format(base_name,cutoff),vmd_script='{0:s}_{1:3.1f}-{2:3.1f}-receptor.vmd'.format(base_name,cutoff),min_rate=None)
        else:
            P.save_receptor_structure('{0:s}_{1:3.1f}-{2:3.1f}-receptor.pdb'.format(base_name,options.threshold,cutoff),vmd_script='{0:s}_{1:3.1f}-{2:3.1f}-receptor.vmd'.format(base_name,options.threshold,cutoff),min_rate=None)
    
    if options.threshold == None:
        P.print_dendrogram(res0=res0,Dcf=options.cutoff,Dtsh=options.threshold,filename='{0:s}_{2:3.1f}-linkage'.format(base_name,cutoff))
    else:
        P.print_dendrogram(res0=res0,Dcf=options.cutoff,Dtsh=options.threshold,filename='{0:s}_{1:3.1f}-{2:3.1f}-linkage'.format(base_name,options.threshold,cutoff))
    
    
    
    with open('{0:s}-rates.txt'.format(options.out),'w') as f_out:
        f_out.write('{0:s}'.format(P))
    
    quit()

