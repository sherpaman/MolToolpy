#!/usr/bin/env python

import MDAnalysis as MD
from argparse import ArgumentParser
parser = ArgumentParser( description = 'Protonate sidechains')
#
# INPUT FILES
#
parser.add_argument("-p","--pdb",dest="pdb",action="store",type=str,default=None,help="Input PROTEIN PDB",required=True,metavar="PDB FILE")
parser.add_argument("-l","--list",dest="list_res",action="store",type=str,default=None,help="File defining the list of GLU residues in the PROTEIN PDB to be modified",required=False,metavar="TXT FILE")
#
# OUTPUT FILES
#
parser.add_argument("-o","--out",dest="out",action="store",type=str,default=None,required=True,help="Output PDB",metavar="PDB FILE")
#
# VAR ARGUMENTS
#
#

options = parser.parse_args()

prot=options.pdb

prot_res = { "ASP":"ASH", "GLU":"GLH", "HIS":"HIP", "LYS":"LYN" }  

resname = [ i.split()[0] for i in open(options.list_res,'r').readlines() ]
resnum  = [ int(i.split()[1]) for i in open(options.list_res,'r').readlines() ]

p = MD.Universe(prot)

for r,n in zip(resname,resnum):
    print ("Renaming RES {0:s}:{1:d} -> {2:s}".format(r,n,prot_res[r]))
    p.residues[n-1].resname = prot_res[r]

p.atoms.write(options.out)

quit()



