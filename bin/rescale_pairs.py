#!/usr/bin/env python

import parmed as pmd
import numpy as np
from math import sqrt
from argparse import ArgumentParser
parser = ArgumentParser( description = 'create a gromacs topology "pairs" directive with the required rescaling. Needed when combining Forcefield with different 1-4 non-bondend rescaling (e.g. AMBER+GLYCAM)')
#
# INPUT FILES
#
parser.add_argument("-i","--itp",dest="itp",action="store",type=str,default=None,help="Input pairs directive",required=True,metavar="ITP FILE")
parser.add_argument("-t","--top",dest="top",action="store",type=str,default=None,help="Input Topology (PRMTOP,TPR,CHARMM : via ParmED package)",required=True,metavar="TOPOL FILE")
#
# OUTPUT FILES
#
parser.add_argument("-o","--out",dest="out",action="store",type=str,default=None,required=True,help="Output File Name",metavar="ITP FILE")
#
# VAR ARGUMENTS
#
parser.add_argument("--lj",dest="lj",action="store",type=float,default=0.5,help="LJ rescale factor")
parser.add_argument("--qq",dest="qq",action="store",type=float,default=5./6.,help="Coulomb rescale factor")
#
options = parser.parse_args()

f_itp=options.itp
f_top=options.top
f_out=options.out
fudge_lj=options.lj
fudge_qq=options.qq

def read_pairs(f_in):
    pairs = []
    f=open(f_in)
    raw_data=f.readlines()
    f.close()
    skip=True
    for line in raw_data:
        l = line.split()
        if (len(l)<2):
            if skip:
                continue
            else:
                break
        if l[1] == "pairs":
            skip=False
            continue
        if skip:
            continue
        if l[0][0] != ";":
            try:
                pairs.append([int(l[0])-1,int(l[1])-1])
            except:
                print(l_last)
                print(l)
                raise
        l_last=l
    return pairs
    

pairs = read_pairs(f_itp)
top = pmd.load_file(f_top)

out=open(f_out,'w')

out.write("[ pairs ]\n")
out.write(";{0:3s} {1:4s} 2 {2:8s} {3:8s} {4:8s} {5:8s} {6:8s} \n".format("a1","a2","fudge_qq","q1","q2","V","W"))

for p in pairs:
    ai,aj = [ a.type for a in [top.atoms[i] for i in p ] ]
    i,j = p
    qi = top.atoms[i].charge
    qj = top.atoms[j].charge
    si = top.atoms[i].sigma/10. # Angstrom to Nanometers
    sj = top.atoms[j].sigma/10. 
    ei = top.atoms[i].epsilon*4.184 # Calories to Joule
    ej = top.atoms[j].epsilon*4.184
    out.write("{0:4d} {1:4d} 2 {2:8.5f} {3:+8.5f} {4:+8.5f} {5:8.5E} {6:8.5E} ; {7:3s}-{8:3s}\n".format(i+1,j+1,fudge_qq,qi,qj,(si+sj)/2.,sqrt(ei*ej)*fudge_lj,ai,aj))

out.close()
quit()

