#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import pandas
import numpy
import scipy.optimize as opt
import scipy.cluster.hierarchy as sch

import MDAnalysis
import MDAnalysis.analysis.distances as dist

from .tools import *

def read_phrases(phrases_file, dist=None, **kwargs):
    """
        Create a phrases object from a save file.
        
        The file must be a pickle file using the same format used 
        by the function ``phrases.save()``. 
        This new format contains all the informations needed to restore 
        the original object.
        
        Arguments
        ---------
        Phrases : str
            A pickle file name. The File should contain a list in the format::
            
                [ list phrases, [ str topology_file, str trajectory_file, ndarray receptor_atoms_list, ndarray ligand_atom_list ] ]
            
            The list is a nested list genretade by the function phrases.find_phrases()
        dist : str, otional
            The file should contain the symmetric matrix of residues "pseudo-distances" as
            calculated by the phrases.calc_dist() function.
        
        Returns
        -------
        out : phrases object
        
    """
    
    try:
        p_file = open(phrases_file,'rb')
        ph, mdata, sysdata = pickle.load(p_file)
    except:
        p_file = open(phrases_file,'rb')
        ph, mdata = pickle.load(p_file)
    p_file.close()
    
    if len(mdata) ==4 :
        top,traj,rec,lig = mdata
    else:
        top,traj,rec,lig,cutoff = mdata
    out = phrases(topol=top,traj=traj,rec=rec,lig=lig,cutoff=cutoff,**kwargs)
    out.phrases = ph
    if numpy.all(out.ligands == None):
            ligands = [ out.ligand.atoms[numpy.where(out.ligand.resids == r)[0]] for r in numpy.unique(out.ligand.resids) ]
            out.ligands = ligands
    if dist!=None:
        out.D = numpy.load(dist)['arr_0']
    else:
        out.calc_dist()
    return out


class phrases:
    """
        .. class:: phrases
            
            A phrases object can be used to store data for the analysis of a Molecular Dymanics of 
            receptor/ligand interaction.
            
            The trajectory must be passed as a MDAnalysis "Universe" object.
            
            Parameters
            ----------
            universe: MDAnalysis Universe
                The Universe should usually contain a MD simulations of a receptor (e.g. a protein) and
                a ligand (e.g. one or more small molecules)
            receptor: MDAnalysis AtomGroup
                The atom group defining the macromolecular receptor (e.g. a protein) must be a AtomGroup subset of ``universe``
            ligand : MDAnalysos AtomGroup
                The atom group defining the ligand(s) must be a AtomGroup subset of ``universe``.
            cutoff : float, optional
                Distance cutoff in Angstrom defining the contact between a ligand and a receptor residue.
            min_len : int, optional
                Minimum number of residues that should be within the cutoff distance of any atom of any ligand in order
                to be recorded as a contact in the ``phrases`` list
            
            Attributes
            ----------
            
            universe : MDAnalysis Universe
            receptor : MDAnalysis AtomGroup
            ligand : MDAnalyisi AtomGroup
            cutoff : float
            min_len : int
            phrases : list
            D : ndarray
                Pseudo-distance between receptor residues
            clusters : list of ndarrays
                each element of the list is an ndarray containing 
                the residues numbers clustered together on the basis of 
                their relative Pseudo-distance
            interface : list of ndarrays
                This attribute can be used to define a list of interface sites 
                in a simulation aimed at the study of interaction between
                macromolecules (e.g. protein/protein, protein/DNA, etc.). 
                Each element of the list is an ndarray containing residues numbers that
                defines interaction sites in the secondary interactor.
            occupancy : ndarray
                3-dim array containing the distance between each binding site 
                present in ``clusters`` and each ligand as a function of time.
                size : [ len(clusters), len(ligand.residues), len(u.trajectory) ]
            interaction : ndarray
                3-dim array containing the distance between each binding site 
                present in ``clusters`` and each interface site in ``interface``, as a function of time.
                size : [ len(clusters), len(ligand.residues), len(u.trajectory) ]
            rate : list
            interaction_rate : list
            analysis_ligand : bool
            analysis_interface : bool
             
            Examples
            --------
            
            The most simple way to create a phrases object from a tajectory is::
            
              >>> import phrases
              >>> import MDAnalysis as MD
              >>> u = MDAnalysis.Universe('topol.tpr','traj.xtc')
              >>> rec = u.select_atoms('protein')
              >>> lig = u.select_atoms('resname LIG')
              >>> P = phrases.phrases(universe=u,receptor=r,ligand=lig)
            
            The typical analysis protocol can be summarized by the following lines of code::
              
              >>> P.find_phrases()     # identify interaction phrases
              >>> P.calc_dist()        # residue pseudo-distance
              >>> P.complete_linkage() # residue clustering
              >>> P.calc_rate()        # occupation and kinetic rate estimate 
              >>> print(P)             
              
    """
    def __init__(self,topol='topol.tpr',traj='traj.xtc',rec=None,lig=None,cutoff=3.5,min_len=3,group='res'):
        self.topol = topol
        self.traj = traj
        self.rec = rec
        self.lig = lig
        self.universe = MDAnalysis.Universe(topol,traj)
        self.receptor = self.universe.select_atoms(rec)
        self.ligand = self.universe.select_atoms(lig)
        self.cutoff = cutoff
        self.min_len = min_len
        self.phrases = []
        self.D = None
        self.occupancy = None
        self.interaction = None
        self.clusters = None
        self.Dcf  = None
        self.Dtsh = numpy.inf
        self.interface = None
        self.ligands = None
        self.dt = None
        self.n_fr = None
        self.skip = 1
        self.rate = None
        self.ACF  = None
        self.interaction_rate = None
        self.analysis_ligand=False
        self.analysis_interface=False
        
    
    def __str__(self):
        """
            The string representation of a phrases object includes informations
            about the trajectory and topology file used and, if present, the kinetic
            rates for ligand bidning/unbinding for each binding site identified and/or 
            the kinetic rates of interaction of interface sites.
             
        """
        min_occ=5.0
        top = self.universe.__dict__['filename']
        trj = self.universe.__dict__['_trajectory'].__dict__['filename']
        HEAD = " PHRASES top:{top:s},trj:{trj:s} \n".format(top=top,trj=trj)
        BODY = ""
        TAIL = ""
        if self.analysis_interface:
            BODY = BODY + "\n--- INTERFACE ANALYSIS ---\n"
            if numpy.all(self.clusters != None):
                for k,c in enumerate(self.clusters):
                    for l,i in enumerate(self.interface):
                        CL = "{0:2d} - {1:2d}".format(k, l)
                        SPACER = "       "
                        if self.interaction_rate[k,l][0] > min_occ:
                            test=True
                            if numpy.all([self.interaction_rate[k,l][1][j] != None for j in range(4)]):
                                test = False
                                CL = CL          + "   ({0:8.2f}) K_ON   : {1:5.2f}ns ({2:7.3f}) ; {3:5.2f}ns ({4:7.3f})\n".format(self.interaction_rate[k,l][0],*self.interaction_rate[k,l][1])
                            if numpy.all([self.interaction_rate[k,l][2][j] != None for j in range(4)]):
                                test = False
                                CL = CL + SPACER + "              K_OFF  : {0:5.2f}ns ({1:7.3f}) ; {2:5.2f}ns ({3:7.3f})\n".format(*self.interaction_rate[k,l][2])
                            if test:
                                CL = CL + "   ({0:8.2f})\n".format(self.interaction_rate[k,l][0])
                        else:
                            CL = CL + "   ({0:8.2f})\n".format(self.interaction_rate[k,l][0])
                        BODY = BODY + CL
        if self.analysis_ligand:
            BODY = BODY + "\n--- LIGANDS ANALYSIS ---\n"
            if numpy.all(self.clusters != None):
                for n,c in enumerate(self.clusters):
                    CL = "{0:3d} ".format(n)
                    BS = ""
                    SPACER = "    "
                    for r in c:
                        BS = BS + "{0:3d} ".format(r)
                        SPACER = SPACER + "    "
                    CL = CL + BS
                    if self.rate[n][0] > min_occ:
                        test_on  = False
                        test_off = False
                        if numpy.all([self.rate[n][1][i] != None for i in range(4)]):
                            CL = CL          + "   ({0:8.2f}) K_ON   : {1:5.2f}ns ({2:7.3f}) ; {3:5.2f}ns ({4:7.3f})\n".format(self.rate[n][0],*self.rate[n][1])
                            test_on = True
                        if numpy.all([self.rate[n][2][i] != None for i in range(4)]):
                            if test_on:
                                CL = CL + SPACER + "              K_OFF  : {0:5.2f}ns ({1:7.3f}) ; {2:5.2f}ns ({3:7.3f})\n".format(*self.rate[n][2])
                            else:
                                CL = CL + SPACER + "   ({0:8.2f}) K_OFF  : {0:5.2f}ns ({1:7.3f}) ; {2:5.2f}ns ({3:7.3f})\n".format(self.rate[n][0],*self.rate[n][2])
                            test_off = True
                        if not (test_on or test_off ):
                            CL = CL + "   ({0:8.2f})\n".format(self.rate[n][0])
                    else:
                        CL = CL + "   ({0:8.2f})\n".format(self.rate[n][0])
                    BODY = BODY + CL
        TAIL = TAIL + "\n"
        return HEAD + BODY + TAIL
    
    def find_phrases(self,b=0,e=-1,s=1,group='res',group_num=None):
        """
            Analyze the trajectory and creates a list of inteaction phrases.
            A phrase is defined a list of residues of the receptor within the cutoff of a ligand's atom.
            The output list is a 3-level nested lists::
            
              phrases[i][j] = list of receptor residues contacting the i-th ligand list element (see grouping options below) in the j-th frame
            
            result are stored in the self.phrases attribute.
            
            Arguments
            ---------
            b : int, optional
            e : int, optional
            s : int, optional
            group : str, optional
                select how ligand atoms are grouped generating a list of elements.
                For each element in the list a phrase is generated for each frame.
                
                Options
                .......
                
                "res"      : Ligand atom list is sub-divided residue-wise. (default)
                "all"      : The ligand group is treated as a single object.
                "atom"     : Each ligand atom is analyzed idependetly 
                            (Warning: This option can cause Memory overflow if not used carfully)
                "num_atom" : ligand atoms are divided in groups of the same size defined by the variable "group_num"
            
            group_num: int, optional
                see "num_atom" option for the "group" variable.
            
            
        """
        self.phrases = []
        print ("Start reading trajectory")
        old_t=0.0
        if group=='res':
            l_group = [ self.ligand.atoms[numpy.where(self.ligand.resids == r)[0]] for r in numpy.unique(self.ligand.resids) ] # create a list of AtomGroups divided by Ligand residues 
        elif group=='all':
            l_group = [ self.ligand ]
        elif group=='atom':
            l_group = [ a for a in self.ligands.atoms ]
        elif group=='num_atom':
            if group_num == None:
                print("When selecting \"num_atom\" grouping you also need to define the \"groum_num\" variable")
                raise ArgumentError
            else:
                g = group_num
                l_group = [ self.ligand.atoms[i:i+g] for i in numpy.arange(len(self.ligand.atoms))[::g] ]
        else:
            print("group should be either \"res\", \"all\", \"atom\" or \"nun_atom\" ")
            raise ValueError
        for self.fr in self.universe.trajectory[b:e:s]:
            self.dt = self.fr.time - old_t
            old_t = self.fr.time
            phrase=[]
            
            for lr in l_group:
                p = list(numpy.unique(self.receptor.select_atoms("around %f global group grp" %(self.cutoff), grp=lr).resids).astype(int) - self.receptor.residues[0].resid)
                if len(p) >= self.min_len:
                    phrase.append(p)
                else:
                    phrase.append([])
            self.phrases.append(phrase)
        print("Done!")
        return

    def find_phrases_parallel(self,b='from the beginning of the trajectory',e='until the end of the trajectory',s=1,group='res',group_num=None):
        """
            Analyze the trajectory and creates a list of inteaction phrases.
            A phrase is defined a list of residues of the receptor within the cutoff of a ligand's atom.
            The output list is a 3-level nested lists::
            
              phrases[i][j] = list of receptor residues contacting the i-th ligand list element (see grouping options below) in the j-th frame
            
            result are stored in the self.phrases attribute.
            
            Arguments
            ---------
            s : int, optional
                skip trajectory every _s_ steps
            group : str, optional
                select how ligand atoms are grouped generating a list of elements.
                For each element in the list a phrase is generated for each frame.
                
                Options
                .......
                
                "res"      : Ligand atom list is sub-divided residue-wise. (default)
                "all"      : The ligand group is treated as a single object.
                "atom"     : Each ligand atom is analyzed idependetly 
                            (Warning: This option can cause Memory overflow if not used carfully)
                "num_atom" : ligand atoms are divided in groups of the same size defined by the variable "group_num"
            
            group_num: int, optional
                see "num_atom" option for the "group" variable.
            
            
        """
        import mdreader
        #print("Using {0}".format(mdreader.__file__))
        reader = mdreader.MDreader()
        reader.setargs(s=self.topol,f=self.traj,b=b,e=e,skip=s)
        reader.opts.asframenum=True
        receptor = reader.universe.select_atoms(self.rec)
        ligand   = reader.universe.select_atoms(self.lig)
        
        
        if group=='res':
            l_group = [ ligand.atoms[numpy.where(ligand.resids == r)[0]] for r in numpy.unique(ligand.resids) ] # create a list of AtomGroups divided by Ligand residues 
        elif group=='all':
            l_group = [ ligand ]
        elif group=='atom':
            l_group = [ a for a in ligands.atoms ]
        elif group=='num_atom':
            if group_num == None:
                print("When selecting \"num_atom\" grouping you also need to define the \"groum_num\" variable")
                raise ArgumentError
            else:
                g = group_num
                l_group = [ ligand.atoms[i:i+g] for i in numpy.arange(len(ligand.atoms))[::g] ]
        else:
            print("group should be either \"res\", \"all\", \"atom\" or \"nun_atom\" ")
            raise ValueError
        
        
        def read_phrase(r,lg,cutoff,min_len):
            phrase=[]
            for lr in lg:
                p = list(numpy.unique(r.select_atoms("around %f global group grp" %(cutoff), grp=lr).resids).astype(int) - r.residues[0].resid)
                if len(p) >= min_len:
                    phrase.append(p)
                else:
                    phrase.append([])
            return phrase
        
        self.phrases=[]
        self.phrases = reader.do_in_parallel(read_phrase,receptor,l_group,self.cutoff,self.min_len)
        
        return 

    def save(self,file_name):
        """
            create a file containing informations to restore a phrases
        """
        import socket, time
        with open(file_name, 'wb') as output:
            mdata   = [os.path.abspath(self.topol),os.path.abspath(self.traj),self.rec,self.lig,self.cutoff]
            sysdata = {"hostname" : socket.gethostname(), "time" : time.ctime() }
            pickle.dump([self.phrases, mdata , sysdata ], output, pickle.HIGHEST_PROTOCOL)
        return
    
    def save_ACF(self,file_name):
        if numpy.all(self.ACF != None):
            # ACF_UBOUND is the Autocorrelation of the Un-Bound state (estimate of K_on)
            # ACF_BOUND  is the Autocorrelation of the    Bound state (estimate of K_off)
            numpy.savez(file_name,acf=self.ACF)
        else:
            print("Autocorrelation Functions have not been calculated")
        return
    
    def load_ACF(self,file_name,force=False):
        if numpy.all(self.occupancy != None):
            print("Impossible to load ACF on and Object that doesn't contains Occupancy data!")
        else:
            n_cl, n_lig, n_fr = self.occupancy.shape
            if numpy.all(self.ACF != None) & force == False:
                print("Object already contains Autocorrelation Functions")
                return
            else:
                # ACF_UBOUND is the Autocorrelation of the Un-Bound state (estimate of K_on)
                # ACF_BOUND  is the Autocorrelation of the    Bound state (estimate of K_off)
                data = numpy.load(file_name)
                ACF=data['acf']
                n_bs,n,p = ACF.shape
                if n_bs != n_cl:
                    print("number of binding sites in ACF is not compatible with occupancy data")
                    return
                if n!=2:
                    print("ACF data is malformed")
                    return
                if p != n_fr:
                    print("number of frames in ACF is not compatible with occupancy data")
                    return
                self.ACF = ACF
                return
        
    def as_dataframe(self,file_name=None):
        dict_frame = { "BSite"    : self.clusters, 
                       "Res_Perc" : [ r[0]    for r in self.rate ],
                       "K_on_1"   : [ r[1][0] for r in self.rate ],
                       "w_on_1 "  : [ r[1][1] for r in self.rate ],
                       "K_on_2"   : [ r[1][2] for r in self.rate ],
                       "w_on_2 "  : [ r[1][3] for r in self.rate ],
                       "K_off 1"  : [ r[2][0] for r in self.rate ],
                       "w_off 1"  : [ r[2][1] for r in self.rate ],
                       "K_off 2"  : [ r[2][2] for r in self.rate ],
                       "w_off_2"  : [ r[2][3] for r in self.rate ] }
        
        self.dataframe=pandas.DataFrame(dict_frame,columns=["BSite","Res_Perc","K_on 1","w_on_1","K_off 1","w_off_1","K_on 2","w_on_2","K_off 2","w_off_2" ])
        
        if file_name != None:
            file_name = os.path.splitext(file_name)[0] + '.csv'
            self.dataframe.to_csv(file_name)
        else:
            return self.dataframe
    
    def save_receptor_structure(self,file_name,vmd_script=None,min_rate=None):
        # ensure we are usind PDB format
        self.receptor.atoms.write(file_name)
        self.receptor = MDAnalysis.Universe(file_name)
        for n,r in enumerate(self.receptor.residues):
            try:
                r.atoms.occupancies = numpy.ones(len(r.atoms))*self.D[n,n]
                r.atoms.tempfactors = numpy.ones(len(r.atoms))*self.labels[n]
            except:
                print ("Error in RES {0} : D = {1:f} , Label = {2:d}".format(r,self.D[n,n],self.labels[n]))
        self.receptor.atoms.write(file_name,bonds=None)
        if vmd_script != None:
            with open(vmd_script,'w') as out_vmd:
                out_vmd.write("color Display Background white\n")
                out_vmd.write("mol new {0:s} type pdb\n".format(file_name))
                out_vmd.write("mol delrep 0 top \nmol representation NewCartoon 0.300000 10.000000 4.100000 0\n")
                out_vmd.write("mol color Occupancy\nmol material Opaque\n")
                out_vmd.write("mol addrep top\n")
                out_vmd.write("mol selupdate 0 top 0\n")
                out_vmd.write("mol colupdate 0 top 0\n")
                for n,c in enumerate(self.clusters):
                    if (min_rate != None):
                        if (self.rate[n][0] < min_rate):
                            continue
                    else:
                        out_vmd.write("mol representation MSMS 1.500000 2.500000 0.000000 0.000000\n")
                        out_vmd.write("mol color ColorID {0:d}\n".format(n))
                        out_vmd.write("mol selection {{not name \"[0-9]?H.*\" and beta {0:d}}}\n".format(n+1))
                        out_vmd.write("mol addrep top\nmol selupdate {0:d} top 0\n".format(n+1))
                        out_vmd.write("mol colupdate {0:d} top 0\n".format(n+1))
        return

    def save_occupancy(self,file_name):
        if numpy.all(self.occupancy != None):
            numpy.savez(file_name,occupancy=self.occupancy,clusters=self.clusters,Dcf=self.Dcf,Dtsh=self.Dtsh)
        else:
            print("Object doesn't contains Occupancy Data")
        return
    
    def load_occupancy(self,file_name,force=False):
        if numpy.all(self.occupancy != None) & force==False:
            print("Object seems ot already contain Occupancy Data: loading aborted!") 
        elif numpy.all(self.clusters == None):
            print("Object doesn't contain any cluster: Before loading Occupancy Data you should have some clusters defined") 
        else:
            fnz = numpy.load(file_name)
            occ  = fnz['occupancy']
            cl   = fnz['clusters']
            Dcf  = fnz['Dcf']
            Dtsh  = fnz['Dtsh']
            n_cl, n_lig, n_fr = occ.shape
            if n_cl != len(cl):
                print("The Occupancy data [n_cl :{0:3d}] is not compatible with this Object: [n_cl :{1:3d}]".format(n_cl,len(self.clusters)))
                return
            if n_lig != len(self.ligands):
                print("The Occupancy data [n_lig :{0:3d}] is not compatible with this Object: [n_lig :{1:3d}]".format(n_lig,len(self.ligands)))
                return
            if n_fr != len(self.phrases):
                print("The Occupancy data [n_fr :{0:3d}] is not compatible with this Object: [n_fr :{1:3d}]".format(n_fr,len(self.phrases)))
                return
            else:
                self.occupancy = occ
                self.clusters  = clusters
                self.Dcf       = Dcf
                self.Dtsh      = Dtsh
        return

    def calc_dist(self,base=10,**kwargs):
        """
            Calculate the pseudo-distance between receptor residues 
            based on the probability to be found in the same phrase
            (i.e. the probability to contact the same ligand at the
            same time)::
            
             D[i,j] = -log( probability to find res i and  res j in the same phrase )
            
             D[i,i] = -log( probability to find res i in a phrase )
            
            Results are stored in the self.D attribute (ndarray) and
            can be used to cluster residues into binding sites using the
            self.complete_linkage method.
              
        """
        if self.phrases == [] :
            self.find_phrases(**kwargs)
        p_a = [] # one dimensional unraveled version of the phrases object
        self.nobs = 0
        for t in self.phrases: # for each frame
            for f in t: # for each phrase found in that frame (one per ligand sub-group, see: self.find_phrases method )
                self.nobs = self.nobs + 1 # add one to the number of observations
                for a in f: # for each (recetor's) residue in the phrase
                    p_a.append(a) 
        C = numpy.ones((max(p_a)+1,max(p_a)+1)) # Initialized to one instead of zeros to avoid infinite distance.
        for t in self.phrases:
            for f in t:
                for i in range(len(f)):
                    C[f[i],f[i]] += 1
                    for j in range(i+1,len(f)):
                        C[f[i],f[j]] += 1
                        C[f[j],f[i]] += 1
        self.D = -numpy.log(C/self.nobs)/numpy.log(base)
        return

    def calc_dist2(self,**kwargs):
        """
            Calculate the pseudo-distance between receptor residues 
            based on the probability to be found in the same phrase
            (i.e. the probability to contact the same ligand at the
            same time)::
            
             D[i,j] = -log( probability to find res i and  res j in the same phrase )
            
             D[i,i] = -log( probability to find res i in a phrase )
            
            Results are stored in the self.D attribute (ndarray) and
            can be used to cluster residues into binding sites using the
            self.complete_linkage method.
              
        """
        if self.phrases == [] :
            self.find_phrases(**kwargs)
        p_a = []
        self.nobs = 0
        for t in self.phrases:
            for f in t:
                self.nobs = self.nobs + 1 
                for a in f:
                    p_a.append(a)
        C = numpy.zeros((max(p_a)+1,max(p_a)+1))
        for t in self.phrases:
            for f in t:
                for i in range(len(f)):
                    C[f[i],f[i]] += 1
                    for j in range(i+1,len(f)):
                        C[f[i],f[j]] += 1
                        C[f[j],f[i]] += 1
        self.D = -numpy.log(C/self.nobs)
        return

    def best_cutoff(self,method="Variance",**kwargs):
        """
            Return a guess for the optimal cut-off value.
            
            The estimate is based on the clustering distorsion::
                The ratio between the average cluster centroid distance 
                and the average full-set distance.
            
            The best cut-off is then identified as the point most distant 
            from the line connecting the extremal points::
                (1 and N cluster at max(cutoff) and min(cutoff) respectively). 
            
            Reference
            ---------
            
            1.Satopaa, V., Albrecht, J., Irwin, D. and Raghavan, B. Finding a "Kneedle" in a Haystack: Detecting Knee Points in System Behavior. in 2011 31st International Conference on Distributed Computing Systems Workshops 166–171 (2011). doi:10.1109/ICDCSW.2011.20
        """
        if numpy.all(self.D == None):
            self.calc_dist()
        if method == "Variance":
            c,nc,d = tools._Variance(self.D)
            best_idx = tools._elbow_var(nc[1:-1],d[1:-1],**kwargs)
        elif method == "CH":
            c,nc,d = tools._CH(self.D)
            best_idx = tools._max_dist_opt(nc[1:-1],d[1:-1],**kwargs)
        elif method == "distortion":
            c,nc,d = tools._distorsion(self.D)
            best_idx = tools._max_dist_opt(nc[1:-1],d[1:-1],**kwargs)
        else:
            print ("Method {0:d} not implemented/recognized".format(method))
            raise ValueError
        
        print("Best-Cutoff is : {0:f} ".format(c[best_idx+1]))
        return c[best_idx+1]

    def find_cluster(self,**kwargs):
        """
            Just a wrapper function to the residues clustering.
            Currently only the complete linkage hierachical clustering is available.
            
            Arguments
            ---------
            The specific (keyworded) arguments change for the particular clustering method
            
                Complete Linkage ( see self.complete_linkage )
                ----------------
                
                Dcf  : float, optional
                Dtsh : float, optional
                
        """
        if numpy.all(self.D == None):
            self.calc_dist()
        if numpy.all(self.occupancy!=None):
            print("Phrases Object already contains occupancy data that would become obsolete:")
            print(" self.occupancy is going to be deleted")
            self.occupancy = None
        self.complete_linkage(**kwargs)
        return
        
    def complete_linkage(self, Dcf=None, Dtsh=None ):
        """
            Cluster residue based on pseudo-distance using the
            Complete-Linkage hierarchical method implemented in scipy.
            
            
            Arguments
            ---------
            Dtsh: float, optional
                Only residues i for which self.D[i,i] < Dtsh are taken
                into account.
            Dcf: float
                The level at which the linkage dedrogram is cut to form the
                binding sites.
            
            Results
            -------
            
            self.clusters: list of ndarray
                List of clusters containing the residues that are
                found together (in couples) in the same phrase.
            
            self.label: ndarray
                An array of size equal to the number of residues in the receptor.
                contains the residue labeling based on the hierarchical clustering
                (i.e. the cluster to which each residue is assigned).
            
            self.linkage: ndarray
                The hierarchical clustering encoded as a linkage matrix.
            
        """
        print("Clustering Residues")
        
        Dcf  = Dcf  or self.Dcf
        Dtsh = Dtsh or self.Dtsh
        if numpy.all(self.D==None):
            self.calc_dist()
        if Dcf == None:
            Dcf = self.best_cutoff(method="CH")
        self.Dcf  = Dcf
        
        # Only residues with a probability > that exp(-threshold) to be 
        # in any pharse are selected and put in a new distance matrix D_t
        print("cut-off: {0:6.2f}\n threshold : {1:6.2f}".format(Dcf, Dtsh))
        if (Dtsh < numpy.inf):
            if Dtsh < numpy.min(numpy.diagonal(self.D)) :
                print("Threshold is too low [{0:.2f}], minimal allowed value for this system is {1:.4f}".format(threshold,numpy.min(numpy.diagonal(self.D))))
                raise ValueError
            t_idx = numpy.where(numpy.diagonal(self.D)<Dtsh)[0]
            D_t = self.D[t_idx][:,t_idx]
            self.res_threshold = t_idx
        else:
            D_t = self.D
            self.res_threshold = numpy.arange(len(self.D))
        self.Dtsh = Dtsh
        
        # Complete Linkage Hierachical Clustering based on "Residue" distance
            
        idx = numpy.triu_indices(D_t.shape[0],1)
        Z = sch.linkage(D_t[idx],method='complete')
        L = sch.fcluster(Z,t=self.Dcf,criterion='distance')
        # The Dendrogram tree is cut at cut-off and the resulting binding-sites ("clusters") are 
        # generated. 
        # The "labels" array reference each receptor residue to its corresponding cluster.
        if self.Dtsh < numpy.inf:
            self.clusters = [ t_idx[numpy.where(L == i )[0]] for i in numpy.arange(max(L))+1 if len(numpy.where(L==i)[0]) >= self.min_len ]
            self.labels = numpy.zeros(self.D.shape[0],dtype=int)
            for n,i in enumerate(t_idx):
                self.labels[i] = L[n]
        else:
            self.clusters = [ numpy.where(L == i )[0] for i in numpy.arange(max(L))+1 if len(numpy.where(L==i)[0]) >= self.min_len ]
            self.labels   = L
        self.linkage = Z
        return
    
    def print_dendrogram(self,size=10,res0=0,Dcf=None,Dtsh=None,filename=None):
        import matplotlib.pyplot as plt
        Dcf  = Dcf  or self.Dcf
        Dtsh = Dtsh or self.Dtsh
        dpi = 96 
        
        if numpy.all(self.D == None):
            self.calc_dist()
        subs=numpy.where(numpy.diag(self.D)< Dtsh)[0]
        if len(subs) < 1:
            print("Threshold ({0:8.3f}) Corresponds to 0 residues".format(Dtsh))
            quit()
        res=numpy.array([ l.resname+str(l.resid+res0) for l in self.receptor.residues])
        
        d_s=self.D[subs][:,subs]
        res_s=res[subs]
        idx = numpy.triu_indices(d_s.shape[0],1)
        # perform two clustering to create two independent copies
        Y1 = sch.linkage(d_s[idx], method='complete')
        Y2 = sch.linkage(d_s[idx], method='complete')
        
        #Create the Canvas
        fig = plt.figure(figsize=(size,size))
        
        fs = min(1.0,dpi*0.9/(2*len(subs))) # font size rescale
        
        # Right side Dendrogram
        ax1 = fig.add_axes([0.8,0.1,0.15,0.6])
        Z1 = sch.dendrogram(Y1, color_threshold=Dcf, orientation='right')
        idx1 = Z1['leaves']
        ax1.set_xticks([])
        ax1.set_yticklabels(res_s[idx1],size=int(fs*fig.get_size_inches()[1]))
        
        # Top side Dendrogram
        ax2 = fig.add_axes([0.09,0.8,0.6,0.15])
        Z2 = sch.dendrogram(Y2, color_threshold=Dcf)
        idx2 = Z2['leaves']
        #ax2.set_xticks([])
        ax2.set_xticklabels(res_s[idx2],size=int(fs*fig.get_size_inches()[1]))
        ax2.plot(ax2.get_xlim(),[Dcf,Dcf],'--',lw=0.25) #draw horizontal line at cutoff
        
        # Reordered Large Distance Matrix
        axmatrix = fig.add_axes([0.09,0.1,0.6,0.6])
        im = axmatrix.matshow(d_s[idx1][:,idx2], aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        im.set_clim(numpy.min(self.D),numpy.max(self.D))
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        
        # Original Distance Matrix (Small)
        axmatrix2 = fig.add_axes([0.74,0.74,0.18,0.18])
        im = axmatrix2.matshow(self.D, aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        
        # Colorbar
        cbaxes = fig.add_axes([0.95,0.1, 0.01, 0.6])
        plt.colorbar(im, cax=cbaxes)
        
        if filename != None:
            plt.savefig('{0:s}.pdf'.format(filename),fmt='pdf')
        else:
            plt.show()
        return
        
    def _dendrogram(self,figure,res0=0,Dcf=None,Dtsh=None,filename=None):
        import matplotlib.pyplot as plt
        Dcf  = Dcf  or self.Dcf
        Dtsh = Dtsh or self.Dtsh
        dpi = 96 
        
        if numpy.all(self.D == None):
            self.calc_dist()
        subs=numpy.where(numpy.diag(self.D)< Dtsh)[0]
        if len(subs) < 1:
            print("Threshold ({0:8.3f}) Corresponds to 0 residues".format(Dtsh))
            quit()
        res=numpy.array([ l.resname+str(l.resid+res0) for l in self.receptor.residues])
        
        d_s=self.D[subs][:,subs]
        res_s=res[subs]
        idx = numpy.triu_indices(d_s.shape[0],1)
        # perform two clustering to create two independent copies
        Y1 = sch.linkage(d_s[idx], method='complete')
        Y2 = sch.linkage(d_s[idx], method='complete')
        
        #Create the Canvas
        fig = figure
        
        fs = min(1.0,dpi*0.9/(2*len(subs))) # font size rescale
        
        # Right side Dendrogram
        ax1 = fig.add_axes([0.8,0.1,0.15,0.6])
        Z1 = sch.dendrogram(Y1, color_threshold=Dcf, orientation='right')
        idx1 = Z1['leaves']
        ax1.set_xticks([])
        ax1.set_yticklabels(res_s[idx1],size=int(fs*fig.get_size_inches()[1]))
        
        # Top side Dendrogram
        ax2 = fig.add_axes([0.09,0.8,0.6,0.15])
        Z2 = sch.dendrogram(Y2, color_threshold=Dcf)
        idx2 = Z2['leaves']
        #ax2.set_xticks([])
        ax2.set_xticklabels(res_s[idx2],size=int(fs*fig.get_size_inches()[1]))
        ax2.plot(ax2.get_xlim(),[Dcf,Dcf],'--',lw=0.25) #draw horizontal line at cutoff
        
        # Reordered Large Distance Matrix
        axmatrix = fig.add_axes([0.09,0.1,0.6,0.6])
        im = axmatrix.matshow(d_s[idx1][:,idx2], aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        im.set_clim(numpy.min(self.D),numpy.max(self.D))
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        
        # Original Distance Matrix (Small)
        axmatrix2 = fig.add_axes([0.74,0.74,0.18,0.18])
        im = axmatrix2.matshow(self.D, aspect='auto', origin='lower', cmap=plt.get_cmap('coolwarm'))
        
        # Colorbar
        cbaxes = fig.add_axes([0.95,0.1, 0.01, 0.6])
        plt.colorbar(im, cax=cbaxes)
        
        return
    
    def calc_occupancy(self,b=0,e=-1,s=1,**kwargs):
        """
            Measure the average (euclidean) distance between each binding site 
            (i.e. each group of receptor residues in self.clusters) 
            and each ligand residue.
            For each residue in a binding site, the minimun distance 
            to each ligand is measured; the average across biding site residues 
            are stored in an array of size [ #Binding_sites, #Ligands, #frames ].
            
            Arguments
            ---------
                
                b : integer, optional
                    First frame to analyze.
                e : integer, optional
                    Last frame to analyze. 
                s: integer, optional
                    The analysis is performed skipping every "s" frames.
            
            Returns
            -------
                
                self.occupancy : ndarray
                    An array of size  [#Binding_sites, #Ligands, #frames ]
                    containing the avreage distance between the residues in a
                    binding site and each ligand residue.
            
            
        """
        print("Calculating Occupancy")
        if numpy.all(self.clusters == None):
            self.complete_linkage(**kwargs)
        
        self.n_fr = self.universe.trajectory.n_frames
        self.skip=s
        self.time = numpy.array([ ts.time for ts in self.universe.trajectory[b:e:s] ])
        nframes = len(self.time)
        res0 = self.receptor.residues[0].resnum
        #
        # 2-dim array bs[i,j] = j-th residue in i-th binding-site
        #
        bs = [ [ self.receptor.select_atoms("resnum {0:d}".format(res_n+res0)) for res_n in site ] for site in self.clusters ]
        n_bs = len(bs)
        
        if numpy.all(self.ligands == None):
            ligands = [ self.ligand.atoms[numpy.where(self.ligand.resids == r)[0]] for r in numpy.unique(self.ligand.resids) ]
            self.ligands = ligands
        n_lig = len(self.ligands)
        
        self.occupancy = numpy.empty([n_bs,n_lig,self.n_fr])
        self.dt = self.universe.trajectory.dt * s
        
        for ts in self.universe.trajectory[b:e:s]:
            for i in range(n_bs):
                M = [ [ numpy.min(dist.distance_array(rec.atoms.positions, lig.positions, backend='OpenMP', box=self.universe.dimensions)) for lig in self.ligands ] for rec in bs[i] ]
                self.occupancy[i,:,ts.frame]  = numpy.mean(M,axis=0)
        return
    
        
    def calc_occupancy_parallel(self,b=0,e=-1,s=1,force=False,**kwargs):
        """
            Measure the average (euclidean) distance between each binding site 
            (i.e. each group of receptor residues in self.clusters) 
            and each ligand residue.
            For each residue in a binding site, the minimun distance 
            to each ligand is measured; the average across biding site residues 
            are stored in an array of size [ #Binding_sites, #Ligands, #frames ].
            
            Arguments
            ---------
                b : integer, optional
                    First frame to analyze.
                e : integer, optional
                    Last frame to analyze.
                s: integer, optional
                    The analysis is performed skipping every "s" frames.
            
            Returns
            -------
                
                self.occupancy : ndarray
                    An array of size  [#Binding_sites, #Ligands, #frames ]
                    containing the avreage distance between the residues in a
                    binding site and each ligand residue.
            
            
        """
        import mdreader
        
        
        if e == -1 :
            e = 'until the end of the trajectory'
            if b == 0:
                b='from the beginning of the trajectory'
                self.time = numpy.array([ ts.time for ts in self.universe.trajectory[::s] ])
            else:
                self.time = numpy.array([ ts.time for ts in self.universe.trajectory[b::s] ])
        else:
            self.time = numpy.array([ ts.time for ts in self.universe.trajectory[b:e:s] ])
        
        
        self.dt = numpy.mean(numpy.diff(self.time))
        
        print("Calculating Occupancy")
        if numpy.all(self.clusters == None):
            self.complete_linkage(**kwargs)
       
        reader = mdreader.MDreader()
        reader.setargs(s=self.topol,f=self.traj,b=b,e=e,skip=s)
        
        receptor = reader.universe.select_atoms(self.rec)
        ligand   = reader.universe.select_atoms(self.lig)
        
        self.n_fr = self.universe.trajectory.n_frames
        res0 = receptor.residues[0].resnum
        #
        # 2-dim array bs[i,j] = j-th residue in i-th binding-site
        #
        bs = [ [ receptor.select_atoms("resnum {0:d}".format(res_n+res0)) for res_n in site ] for site in self.clusters ]
        n_bs = len(bs)
        
        if numpy.all(self.ligands == None):
            ligands = [ ligand.atoms[numpy.where(ligand.resids == r)[0]] for r in numpy.unique(ligand.resids) ]
            self.ligands = ligands
        n_lig = len(self.ligands)
        
        
        def occ():
            occupancy = numpy.zeros([n_bs,n_lig])
            for i in range(n_bs):
                M = [ [ numpy.min(dist.distance_array(rec.atoms.positions,lig.positions,box=self.universe.dimensions)) for lig in self.ligands ] for rec in bs[i] ]
                occupancy[i]  = numpy.mean(M,axis=0)
            return occupancy
        
        occupancy = numpy.array(reader.do_in_parallel(occ))
        self.occupancy=numpy.rollaxis(occupancy,0,3)
        
        return
    
    def calc_interaction(self,b=0,e=-1,s=1):
        """
            Measure the average (euclidean) distance between each binding site 
            (i.e. each group of receptor residues in self.clusters) 
            and each Interface.
            
            For each residue in a binding site, the minimun distance to each residue 
            in another interface site is measured; the average among all residues
            is stored in an array of size [ #Binding_sites, #Interface_sites, #frames ].
            
            If both the binding site and the interface site are formed by more than 3 residues
            the average distance for the binding site is weight-averaged with 
            the average distance for the interface sites (using respective variance as weight)
            
            Arguments
            ---------
                
                b : integer, optional
                    First frame to analyze.
                e : integer, optional
                    Last frame to analyze. 
                s: integer, optional
                    The analysis is performed skipping every "s" frames.
            
            Returns
            -------
                
                self.interaction : ndarray
                    An array of size  [#Binding_sites, #Interface_sites, #frames ]
                    containing the average distance between the residues in a
                    binding site and the residues in an Interface Site.
                    
            
            
        """
        if numpy.all(self.interface == None):
            print("self.interface not defined. Before proceeding you need to define a list (of at least one) of interfaces")
            return
        print("Calculating Interaction")
        if numpy.all(self.clusters == None):
            self.complete_linkage(**kwargs)
        
        self.n_fr = self.universe.trajectory.n_frames
        self.time = numpy.array([ ts.time for ts in self.universe.trajectory[b:e:s] ])

        #
        # 2-dim array bs[i,j] = j-th residue in i-th binding-site
        #
        
        bs1 = [ [ self.receptor.select_atoms("resnum {0:d}".format(res_n)) for res_n in site ] for site in self.clusters ]
        n_bs1 = len(bs1)
        
        bs2 = [ [ self.universe.select_atoms("resnum {0:d}".format(res_n)) for res_n in site ] for site in self.interface ]
        n_bs2 = len(bs2)
        
        self.interaction = numpy.empty([n_bs1,n_bs2,self.n_fr])
        self.dt = self.universe.trajectory.dt * s
        for ts in self.universe.trajectory[b:e:s]:
            for i in range(n_bs1):
                for j in range(n_bs2):
                    M = [ [ numpy.min(dist.distance_array(rec.positions,lig.positions,box=self.universe.dimensions)) for lig in bs2[j] ] for rec in bs1[i] ]
                    d = [ numpy.mean(numpy.min(M,axis=ax)) for ax in range(2) ] # Average minumum distance per each residue in Receptor and Ligand
                    if (len(bs1[i])<3) | (len(bs2[j])<3):
                        self.interaction[i,j,ts.frame]  = numpy.average(d)
                    else:
                        # ... Variance Weighted Average ...
                        w = [  numpy.var(numpy.min(M,axis=ax)) for ax in range(2) ] # Variance
                        self.interaction[i,j,ts.frame]  = numpy.average(d,weights=w)
        return
        
    def calc_rate(self,smooth=1,dist_cutoff=None,**kwargs):
        """
            Fit the Auto Correlation Function (acsf) of the binding and unbinding process
            ( derived from the self.occupancy data ) with a double exponential function .
            
            The self.rate attibute will store the total Occupancy percentage for each binding site
            and estimate of Bindin and unbinding rates (K_on and K_off)  derived from the exponential fitting.
            For each binding site 2 K_on and 2 K_off values are recorded (together with their relative weight)
            to possibly detect both fast (or "burst") and slow (or "cluster") events. 
            
            Arguments
            ---------
            
                smooth : integer, optional
                    width of self.occupancy running average
                dist_cutoff : float, optional
                    distance below which the self.oppupacy values are assiged to be bound states
            
            Returns
            -------
            
                self.rate : list
                    List of rate data (one element per binding site in the followinf format::
                        
                        self.rate[i] = [ Occupancy Percentage of site i ,[ data_on ], [ data_off ]
                        data_on = [ K_on_1  , (weight) , K_on_2 , (1.0 - weight) ]
                        data_on = [ K_off_1 , (weight) , K_off_2, (1.0 - weight) ]
                        
                self.analysis_rate : bool
                    after execution is set to True and will trigger associated actions
                    (e.g. printing rates when appropriate)
            
            References
            ----------
            1.  C Arnarez, JP Mazat, J. Elezgaray, SJ Marrink, and X Periole
                Evidence for Cardiolipin Binding Sites on the Membrane-Exposed Surface of the Cytochrome bc1
                Journal of the American Chemical Society 2013 135 (8), 3112-3120 DOI: 10.1021/ja310577u 
                
        """
        if dist_cutoff == None:
            dist_cutoff=self.cutoff
        min_occ = 5.0 # minimum occupancy percentage to trigger analysis 
        if numpy.all(self.occupancy==None):
            self.calc_occupancy(**kwargs)
        else:
            print ("Old occupancy data will be used: make sure that clusters have not been changed since")
        
        conv = numpy.ones(smooth)
        n_bs, n_lig, nframes = self.occupancy.shape
        
        self.rate=numpy.zeros(n_bs,dtype=type([]))
        self.ACF = numpy.zeros((n_bs,2,nframes))
        for i in range(n_bs):
            comb_occ = 100.0 * float(numpy.sum(numpy.any(self.occupancy[i] < dist_cutoff,axis=0)))/len(self.occupancy[i,0])
            # Intialize the binding/ubinding traces.
            # Since we are gonna trim any bound or 
            # ubound state from the start and the end, the first frame is allways a "zero"
            bind=numpy.zeros(1,dtype=int)
            ubind=numpy.zeros(1,dtype=int)
            there_is_rate = False
            for j in range(n_lig):
                if smooth > 1:
                    # calculate running average
                    running = numpy.convolve(self.occupancy[i,j],conv,"valid")/smooth
                else:
                    # use raw data
                    running = self.occupancy[i,j]
                occ=100.0 * float(numpy.sum(running<dist_cutoff))/len(running)
                # only analyze traces with al least min_occ of occupied frames
                if occ > min_occ:
                    # if at least one of the traces for this binding site show min_occ
                    there_is_rate=True
                    # trasforme the occupancy data into a binary state trace (1=bound, 0=unbound)
                    b = (running<dist_cutoff).astype(int)
                    # create the opposite trace
                    ub = 1 - b
                    # trim ones from both ends
                    # and concatenate 
                    bind  = numpy.concatenate([bind ,1 - numpy.trim_zeros(1-b )])
                    ubind = numpy.concatenate([ubind,1 - numpy.trim_zeros(1-ub)])
            
            if there_is_rate:
                acsf_on   = tools._acfs(ubind)
                acsf_off  = tools._acfs(bind)
                np_off, np_on = min((nframes,len(acsf_off))), min((nframes,len(acsf_on)))
                self.ACF[i,0,:np_on]=acsf_on[:np_on]
                self.ACF[i,1,:np_off]=acsf_off[:np_off]
                
                try:
                    l_off = _opt_tau_2(acsf_off[:np_off/10])
                    l_on  = _opt_tau_2(acsf_on[:np_on/10])
                except:
                    print ("Not Possible to estimate rate : [binding site :{0:d}]".format(i) )
                    self.rate[i] = [ comb_occ , [ None for k in range(4) ], [ None for k in range(4) ] ]
                    continue
                #
                t1_on = (1./l_on[0]) * self.dt * self.skip / 1000.0 
                t2_on = (1./l_on[1]) * self.dt * self.skip / 1000.0
                p1_on = 100*l_on[2]
                p2_on = 100-p1_on
                #
                t1_off = (1./l_off[0]) * self.dt * self.skip / 1000.0
                t2_off = (1./l_off[1]) * self.dt * self.skip / 1000.0
                p1_off = 100*l_off[2]
                p2_off = 100-p1_off
                self.rate[i] = [ comb_occ, [t1_on, p1_on, t2_on, p2_on] ,[t1_off, p1_off, t2_off, p2_off] ] 
            else:
                self.rate[i] = [ comb_occ , [ None for k in range(4) ], [ None for k in range(4) ] ]
        self.analysis_ligand = True
        return

    def analyze_interface(self,smooth=2,dist_cutoff=3.5,**kwargs):
        """
            Fit a double exponential function to the Auto Correlation Function (acsf) 
            for the interaction of binding sites (usually on une protein) with other 
            interaction sites (usually found in another protein).
            
            The self.interaction_rate attibute will store the total interaction percentage for each binding site
            and estimate of binding and unbinding rates (K_on and K_off)  derived from the exponential fitting.
            For each binding site 2 K_on and 2 K_off values are recorded (together with their relative weight)
            to possibly detect both fast (or "burst") and slow (or "cluster") events. 
            
            Arguments
            ---------
            
                smooth : integer, optional
                    width of self.occupancy running average
                dist_cutoff : float, optional
                    distance below which the self.oppupacy values are assiged to be bound states
            
            Returns
            -------
            
                self.interaction_rate : list
                    List of rate data (one element per binding site in the followinf format::
                        
                        self.rate[i] = [ Occupancy Percentage of site i ,[ data_on ], [ data_off ]
                        data_on = [ K_on_1  , (weight) , K_on_2 , (1.0 - weight) ]
                        data_on = [ K_off_1 , (weight) , K_off_2, (1.0 - weight) ]
                        
                self.analysis_interface : bool
                    after execution is set to True and will trigger associated actions
                    (e.g. printing rates when appropriate)
                    #395AF0#395AF0
        """
        min_occ=5.0
        if numpy.all(self.interaction==None):
            self.calc_interaction(**kwargs)
        
        conv = numpy.ones(smooth)
        n_bs1, n_trj, n_bs2, nframes = self.interaction.shape
        
        if self.dt == None:
            self.dt = self.universe.trajectory.dt * s
        
        self.interaction_rate=numpy.zeros([n_bs1,n_bs2],dtype=type([]))
        
        for i in range(n_bs1):
            there_is_rate = False
            for j in range(n_bs2):
                bind=numpy.zeros(1,dtype=int)
                ubind=numpy.zeros(1,dtype=int)
                for trj in range(n_trj):
                    if smooth > 1:
                        running = numpy.convolve(self.interaction[i,trj,j],conv,"valid")/smooth
                    else:
                        running = self.interaction[i,trj,j]
                    occ=100.0 * float(numpy.sum(running<dist_cutoff))/len(running)
                    if occ > min_occ:
                        there_is_rate=True
                        b = (running<dist_cutoff).astype(int)
                        ub = 1 - b
                        bind  = numpy.concatenate([bind ,1 - numpy.trim_zeros(1-b )])
                        ubind = numpy.concatenate([ubind,1 - numpy.trim_zeros(1-ub)])
                acsf_off = tools._acfs(bind)
                acsf_on  = tools._acfs(ubind)
                try:
                    l_off = _opt_tau_2(acsf_off)
                    l_on  = _opt_tau_2(acsf_on)
                except:
                    print ("Error analysis : [bs:{0:d} - lig:{1:d}]".format(i,j) )
                    self.interaction_rate[i,j] = [ occ , [ None for k in range(4) ], [ None for k in range(4) ] ]
                    continue
                t1_on = (1./l_on[0]) * self.dt / 1000.0 
                t2_on = (1./l_on[1]) * self.dt / 1000.0
                p1_on = 100*l_on[2]
                p2_on = 100-p1_on
                
                t1_off = (1./l_off[0]) * self.dt / 1000.0
                t2_off = (1./l_off[1]) * self.dt / 1000.0
                p1_off = 100*l_off[2]
                p2_off = 100-p1_off
                self.interaction_rate[i,j] = [ occ, [t1_on, p1_on, t2_on, p2_on] ,[t1_off, p1_off, t2_off, p2_off] ] 
            else:
                self.interaction_rate[i,j] = [ occ , [ None for k in range(4) ], [ None for k in range(4) ] ]
        self.analysis_interface = True
        return
