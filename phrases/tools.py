import pandas
import numpy
import scipy.optimize as opt
import scipy.cluster.hierarchy as sch


def _acfs(data):
    """
        Calculate Auto-Correlation Function of data.
        
        Arguments
        ---------
        
        data: ndarray
            A 1-dim array containing the occupancy state of a binding site for a single ligand::
               data[n] == 1  ==> the site was occupied at frame n
               data[n] == 0  ==> the site was not occupide at frame n
        
        Returns
        -------
        
        acfs: ndarray
            Normalized autocorrelation function
       
    """
    acfs = numpy.zeros(len(data))
    convolve_array = numpy.ones(2, dtype = numpy.int32)
    reduced_dat = numpy.copy(data)
    # first value is the sum of all single-frame occupancy
    # if the ACF reached 0 already, no need to compute the rest
    acfs[0] = float(numpy.sum(reduced_dat))
    if acfs[0] == 0:
        return acfs
    for k in range(1,len(data)):
        if not acfs[k-1]:
            acfs[k:] = 0
            break
        # ACF at this point
        reduced_dat = numpy.convolve(reduced_dat, convolve_array, "valid") == 2
        acfs[k] = float(numpy.sum(reduced_dat))
    # normalization
    acfs = acfs/acfs[0]
    return acfs

def _exp2(x,l1,l2,a):
    """
        Double exponential function uesd to fit ACSF and estimate rate constants.
        
        Arguments
        ---------
        
        x: ndarray
            Evaluate function for these values
        l1,l2 : float
            exponents of the two exponential function
        a :float {0<=a<=1}
            relative weigth of the first exponential function
            
        Returns
        _______
        
        out: ndarray
            the value of function on x
            
    """   
    out = a * numpy.exp(-l1*x) + (1-a) * numpy.exp(-l2*x)
    return out

def _opt_tau_2(a,nz=None):
    """
        Accessory function.
        
        Perform the fitting of _exp_2 function to the acsf::
            ``_exp_2(x,tau_1,tau_2,a)``
        
        Arguments
        ---------
            a: ndarray
                array containg the data 
            
            nz: int, optional
                fit using only the first nz elements of "a". 
                If not given all data is fitted
        
        Returns
        -------
            l : ndarray
                optimized parameters::
                    l[0] = tau_1 (time constant of the first  exponential decay function)
                    l[1] = tau_2 (time constant of the second exponential decay function)
                    l[2] = a     (relative weight of the first exponential decay function)
    """
    if nz == None:
        nz = len(a)
    l0 = a[0] - a[1]
    l, o = opt.curve_fit(_exp2,numpy.arange(nz),a[:nz],p0=[l0,l0/100.,0.5],bounds=( [0.,0.,0.],[numpy.inf, numpy.inf,1.0] ))
    return l

def _distorsion(D):
    idx = numpy.triu_indices_from(D,1)
    ref = numpy.mean(D[idx])
    Z   = sch.complete(D[idx])
    c   = numpy.unique(Z[:,2]) # all merging levels will be explored as potential optimal cutoff
    d   = numpy.zeros(len(c))  # distorsion 
    nc  = numpy.zeros(len(c))
    for n,i in enumerate(c[:-1]):
        l     = sch.fcluster(Z,t=i,criterion='distance') # cluster at level "i" 
        cl    = [ numpy.where(l == k)[0] for k in numpy.unique(l) if len(numpy.where(l == k)[0]) ] # create the clusters
        nc[n] = len(cl) # number of clusters
        # centroids are residues with the lowest average distance within the cluster
        centroids = [ k[numpy.argmin(numpy.mean(D[k][:,k],axis=0))] for k in cl ] 
        c_dist    = D[centroids][:,centroids] # slice of the matrix containing just the centroids
        c_idx     = numpy.triu_indices_from(c_dist,1) 
        d[n]      = numpy.mean(c_dist[c_idx])/ref # average distance between centroids 
    return c[:-1],nc,d
    
def _CH(D):
    """
        Calinski-Harabasz index
    """
    idx = numpy.triu_indices_from(D,1)
    C0  = numpy.argmin(numpy.diagonal(D)) # Data-Set Baricentre 
    Z   = sch.complete(D[idx])
    c   = numpy.unique(Z[:,2]) # all merging levels will be explored as potential optimal cutoff
    d   = numpy.zeros(len(c))  # CH-index 
    nc  = numpy.zeros(len(c),dtype=int)
    for n,i in enumerate(c[:-1]):
        l     = sch.fcluster(Z,t=i,criterion='distance') # cluster at level "i" 
        cl    = [ numpy.where(l == k)[0] for k in numpy.unique(l) if len(numpy.where(l == k)[0]) ] # create the clusters
        nc[n] = len(cl) # number of clusters
        c_sz = numpy.array([ len(k) for k in cl ]) # clusters size
        # centroids are residues with the lowest average distance within the cluster
        centroids = [ k[numpy.argmin(numpy.mean(D[k][:,k],axis=0))] for k in cl ] 
        BGSS = numpy.dot(c_sz,D[C0][centroids]**2)                                    # Between Groups Squared Scatter
        WGSS = numpy.sum([ numpy.sum(D[centroids[i]][cl[i]]**2) for i in range(nc[n]) ]) # Within Groups Squared Scatter
        d[n] = (len(D) - nc[n] ) * BGSS / ( (nc[n] -1) * WGSS ) 
    return c[:-1],nc,d

def _Variance(D):
    """
        Explained Variance
    """
    idx = numpy.triu_indices_from(D,1)
    Z   = sch.complete(D[idx])
    c   = numpy.unique(Z[:,2]) # all merging levels will be explored as potential optimal cutoff
    d   = numpy.zeros(len(c))  # variacne 
    nc  = numpy.zeros(len(c),dtype=int)
    for n,i in enumerate(c[:-1]):
        l     = sch.fcluster(Z,t=i,criterion='distance') # cluster at level "i" 
        cl    = [ numpy.where(l == k)[0] for k in numpy.unique(l) if len(numpy.where(l == k)[0]) ] # create the clusters
        nc[n] = len(cl) # number of clusters
        c_sz = numpy.array([ len(k) for k in cl ]) # clusters size
        # centroids are residues with the lowest average distance within the cluster
        centroids = [ k[numpy.argmin(numpy.mean(D[k][:,k],axis=0))] for k in cl ] 
        for i,cent in enumerate(centroids): 
            d[n] = d[n] + numpy.average(D[centroids[i]][cl[i]]**2)
    d = numpy.sqrt(d)/numpy.sqrt(d[0])
    return c[:-1],nc,d

def _max_dist_opt(x,y,wind=3):
    """
        
    """
    from scipy.stats import linregress as lr
    x1 , y1 = x[:wind]  , y[:wind]
    x2 , y2 = x[-wind:], y[-wind:]
    
    a = lr(numpy.concatenate([x1,x2]),numpy.concatenate([y1,y2]))
    
    b = numpy.array([numpy.cos(numpy.arctan(a[0])),numpy.sin(numpy.arctan(a[0]))])
    p0=numpy.mean([x1,y1],axis=1)
    
    d = numpy.array([ numpy.linalg.norm( (p-p0) - numpy.dot((p-p0),b)*b ) for p in zip(x,y) ])
    
    best_x=numpy.argmax(d[wind:-wind])
    
    #plt.plot(x[:best_x],y[:best_x],'.r')
    #plt.plot(x[best_x:],y[best_x:],'.b')
    #plt.plot(x,a[1]+a[0]*x,'g')
    return best_x

def _elbow_var(x,y,wind=3):
    """
        
    """
    from scipy.stats import linregress as lr
    x1 , y1 = x[:1]  , y[:1]
    x2 , y2 = x[-1:], y[-1:]
    
    a = lr(numpy.concatenate([x1,x2]),numpy.concatenate([y1,y2]))
    
    b = numpy.array([numpy.cos(numpy.arctan(a[0])),numpy.sin(numpy.arctan(a[0]))])
    p0=numpy.mean([x1,y1],axis=1)
    
    dist = numpy.array([ numpy.linalg.norm( (p-p0) - numpy.dot((p-p0),b)*b ) for p in zip(x,y) ])
    
    best_x=numpy.argmax(dist[2:-2])+2
    
    return best_x
