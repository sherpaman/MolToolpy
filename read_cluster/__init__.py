#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import re 

def find_ncl(data):
    n_clust = re.compile('Found ([0-9]+) clusters')
    for i,d in enumerate(data):
        if (len(d.split())>0):
            if d.split()[0] == "Found":
                return int(n_clust.findall(d)[0])
    return None

def find_start(data):
    for i,d in enumerate(data):
        if (len(d.split())>0):
            if d.split()[0] == "cl.":
                return i+1
    return None

def extract_cl(data,s0):
    h_parse = re.compile("([0-9,\ ]+)\|([0-9,\.,\ ]+)\|([0-9\.,\ ]+)\|[\ ,0-9]+")
    p_parse = re.compile("[\ ]*([0-9]+)[\ ]*([\.,0-9]+)[\ ]*")
    if len(data[s0].split()) == 7: # SIZE 1 CLUSTER
        return data[s0].split()[4] , [data[s0].split()[-1]], 0.0, s0+1
    h = h_parse.findall(data[s0])[0]
    p1 = p_parse.findall(h[1])[0]
    p2 = p_parse.findall(h[2])[0]
    head = data[s0].split()
    cl_sz = int(p1[0])
    #bcv   = float(p1[1]) # Between Cluster Variance
    cent  = int(p2[0])
    wcv   = float(p1[1]) # Within Cluster
    if cl_sz >= 7:
        cl = [ int(n) for n in head[-7:] ]
    else:
        cl = [ int(n) for n in head[-cl_sz:] ]
        return cent, cl, wcv, s0+1
    for i in range(1,(cl_sz-1)/7+1):
        line = data[s0+i].split()
        cl_l = [ int(n) for n in line[3:] ]
        cl = cl + cl_l
    return cent, cl, wcv, s0+(cl_sz-1)/7+1

def read_cluster(f_in):
    with open(f_in,'r') as f:
        data = f.readlines()
        n_cl = find_ncl(data)
        s0 = find_start(data)
        clusters = []
        centers  = []
        wcvs     = []
        for c in range(n_cl):
            #print("Cluster {0:4d} starts at line {1:4d}".format(c+1,s0+1))
            try:
                center, cl, wcv, s0 = extract_cl(data,s0)
            except:
                print("Problem Reading line {0} in file".format(s0+1))
                print(extract_cl(data,s0))
                raise
            centers.append(center)
            clusters.append(cl)
            wcvs.append(wcv)
            
    return centers, clusters , wcvs


if __name__ == '__main__':
    file_in="cluster.log"
    file_data="DIST.txt"
    centers, clusters=read_cluster(file_in)
    data=np.loadtxt(file_data,usecols=[2])
    plt.plot(data,lw  =0.2)
    plt.show()
    plt.hist(data,bins=np.arange(20,70),histtype="step",alpha=0.5,density=True,label="Data")
    for n,cl in enumerate(clusters):
        if len(cl)>200:
            m = np.average(data[cl])
            s = np.std(data[cl])
            sz= len(cl)
            print ("{0:d} {1:8.3f} {2:8.3f} ({3:5d})".format(n,m,s,sz))
            plt.hist(data[cl],bins=np.arange(20,70),histtype="step",alpha=0.5,density=True,label="Cluster {0:d}".format(n))
    plt.legend()
    plt.show()
    exit()
